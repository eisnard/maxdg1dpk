c----------------------------------------------------------------------
c                  Matrices et parametres schema P2
c                           Base Lagrange
c----------------------------------------------------------------------
c     Nombre de degres de liberte element : ndl
c     Nombre de degres de liberte face    : ndf
c----------------------------------------------------------------------
      INTEGER ndf, ndl
      PARAMETER (ndl = 3, ndf = 1)
c----------------------------------------------------------------------
c     Matrice de masse multipliee par cmr pour simplifier
c     A diviser par cmr
c
      REAL cmr
      REAL mass(ndl,ndl)
c
      DATA mass/ 
     &     4,  2, -1, 
     &     2, 16,  2, 
     &    -1,  2,  4/
c
      DATA cmr/30/
c----------------------------------------------------------------------
c     Matrice de masse inverse multipliee par cvr pour simplifier
c     A diviser par cvr
c
      REAL cvr
      REAL amat(ndl,ndl)
c
      DATA amat/ 
     &     36, -6,  12,
     &    -6,   9,  -6,
     &     12, -6,  36/
c
      DATA cvr/4/
c----------------------------------------------------------------------
c     Matrice des Phi_j*Dx(Phi_k) (rangement en colonne)
c     A diviser par cgr
c
      REAL cgr
      REAL rdx(ndl,ndl)
c
      DATA rdx/
     &    -3,  4, -1,
     &    -4,  0,  4,
     &     1, -4,  3/
c
      DATA cgr/6/
c----------------------------------------------------------------------
c     Matrices de bords 
c     A diviser par cms
c
      REAL ms1(ndl,ndl), ms2(ndl,ndl), ms3(ndl,ndl), ms4(ndl,ndl)
c
      REAL cms
      DATA cms/1/
c
      DATA ms1/ 
     &     0, 0, 0, 
     &     0, 0, 0,
     &     1, 0, 0/
c
      DATA ms2/ 
     &     1, 0, 0,
     &     0, 0, 0, 
     &     0, 0, 0/
c
      DATA ms3/ 
     &     0, 0, 0,
     &     0, 0, 0,
     &     0, 0, 1/
c
      DATA ms4/ 
     &     0, 0, 1,
     &     0, 0, 0,
     &     0, 0, 0/
c----------------------------------------------------------------------
