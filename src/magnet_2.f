c----------------------------------------------------------------------
      SUBROUTINE MAGNET_2(dt, t_e, ua, un)
c----------------------------------------------------------------------
c     Mise a jour du champ magnetique, schema saute-mouton d'ordre 2
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER nx
      INTEGER npi(0:nxmax)
c
c---- Reels
      REAL dt, t_e
      REAL dxi(0:nxmax), flux(ndl,nxmax), 
     &     ua(2,ndl,0:nx), un(2,ndl,0:nx)
c
      COMMON/dxi/dxi
      COMMON/flux/flux
      COMMON/nx/nx
      COMMON/npi/npi
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, j, k
c
c---- Reels
      REAL dtf
      REAL fld(ndl), flg(ndl), flu(ndl)
c----------------------------------------------------------------------
c
      DO k=1,ndl
         DO i=1,nx
            flux(k,i) = 0.0
         ENDDO
      ENDDO
c
c---- Integrale sur les elements 
c     
      DO i=1,nx-1 
         DO k=1,ndl
            flu(k) = 0.0
c
            DO j=1,ndl
               flu(k) = flu(k) + ua(2,j,i)*rdx(k,j)
            ENDDO
c
            flux(k,i) = flux(k,i) - flu(k)/cgr
         ENDDO
      ENDDO
c
c---- Termes d'interface [i:i+1]
c
      DO i=1,nx-1
         DO k=1,ndl
            fld(k) = 0.0
            flg(k) = 0.0
c
            DO j=1,ndl
               fld(k) = fld(k) + ua(2,j,i)*ms3(k,j) +
     &                           ua(2,j,i+1)*ms4(k,j)
               flg(k) = flg(k) + ua(2,j,i-1)*ms1(k,j) +
     &                           ua(2,j,i)*ms2(k,j)
            ENDDO
c
            flux(k,i) = flux(k,i) + 0.5*fld(k)*npi(i+1)/cms
            flux(k,i) = flux(k,i) - 0.5*flg(k)*npi(i-1)/cms
         ENDDO
      ENDDO
c
c---- Conditions aux bords 
c
      CALL H_BORDS(ua)
c
c---- Multiplication par la matrice de masse inverse
c
      DO i=1,nx-1 
         dtf = dt/(dxi(i)*cvr)
c
         DO k=1,ndl
            flu(k) = 0.0
c
            DO j=1,ndl
               flu(k) = flu(k) + amat(k,j)*flux(j,i)
            ENDDO
c
            un(1,k,i) = ua(1,k,i) + dtf*flu(k)
         ENDDO
      ENDDO
c
c---- Calcul energie
c
      CALL ENERGY(t_e, ua, un)
c
c---- Mise a jour
c
      DO k=1,ndl
         DO i=1,nx
            ua(1,k,i) = un(1,k,i)
         ENDDO
      ENDDO
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
