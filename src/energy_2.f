c----------------------------------------------------------------------
      SUBROUTINE ENERGY_2(t_e, ua, bn)
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
c     Calcul energie discrete
c     Saute-mouton   D_n*D_n + B_(n+1/2)*B(n-1/2)
c     Rkk            D_n*D_n + B_n*B_n
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER isort, kt, nx
c
c---- Reels
      REAL t_e
      REAL dxi(0:nxmax), epsil_r(nxmax), ua(2,ndl,0:nx), bn(ndl,0:nx)
c
      COMMON/dxi/dxi
      COMMON/epsil/epsil_r
      COMMON/isort/isort
      COMMON/kt/kt
      COMMON/nx/nx
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, j, k
c
c---- Reels
      REAL enrj, som
      REAL somE(ndl), somH(ndl)
c----------------------------------------------------------------------
c
      enrj = 0.0
c
      DO i=1,nx-1
         som = 0.0
c
         DO k=1,ndl
            somH(k) = 0.0
            somE(k) = 0.0
c
            DO j=1,ndl
               somH(k) = somH(k) + mass(k,j)*ua(1,j,i)
               somE(k) = somE(k) + mass(k,j)*ua(2,j,i)
            ENDDO
c
            som = som + somH(k)*bn(k,i) +
     &                  somE(k)*ua(2,k,i)*epsil_r(i)
         ENDDO
c
         enrj = enrj + dxi(i)*som
      ENDDO
c
      enrj = 0.5*enrj/cmr
c
      WRITE(12, 10) t_e, enrj
c
      IF (MOD(kt, isort) .EQ. 0) THEN
         WRITE(6, *) ' Temps : ', t_e
         WRITE(6, *) ' Energie approchee : ', enrj
      ENDIF
c
10    FORMAT(2(f15.6, 1x))
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
