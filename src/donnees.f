c----------------------------------------------------------------------
      SUBROUTINE DONNEES(cfl, dt, freq, ktmax, tmax)
c----------------------------------------------------------------------
c---- Parametres lus dans Data_Num
c
c    nlpf      : ordre du schema saute-mouton (=2 ou 4)
c    nrk       : nombre de pas de la methode Rungte-Kutta
c    nx        : nombre points de discretisation
c    rlx       : longueur du domaine
c    xor       : origine du domaine
c    iraf      : type de raffinement 
c    nxd, nxf  : sous-grille a raffiner 
c    irfx      : taux de raffinement 
c    cfl       : nombre de courant 
c    tmax      : temps maximal a atteindre
c    ktmax     : nombre maximal d'iterations en temps 
c    grafx     : visu en x par Dislin (si 0 visu par Gnuplot)
c    nhist     : pour le calcul de la solution en un point
c    freq      : frequence de l'onde incidente ou initiale
c    xv        : abscisse du point de visualisation
c    eps_2     : epsilon relatif  pour cas test 3 
c    icas      : numero du cas test 
c    isort     : pour la sauvegarde/edition des resultats 
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
      INCLUDE 'gauss_10pts.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER grafx, icas, iraf, isort, ktmax, mxv, nhist, nlpf,
     &        nordre, nrk, nx
      INTEGER npi(0:nxmax)
c
c---- Reels
      REAL cfl, dt, eps_2, freq, rlx, xor, tmax, vit0, xk,b,fm,L_slab
      REAL slab_pos
      REAL ark(6), coor(nxmax), dxi(0:nxmax)
c
      DATA vit0/0.30/
c
      COMMON/ark/ark
      COMMON/coor/coor
      COMMON/dxi/dxi
      COMMON/eps_2/eps_2
      COMMON/fm/fm
      COMMON/b/b
      COMMON/L_slab/L_slab
      COMMON/slab_pos/slab_pos
      COMMON/grafx/grafx
      COMMON/icas/icas
      COMMON/iraf/iraf
      COMMON/isort/isort
      COMMON/nhist/nhist
      COMMON/nlpf/nlpf
      COMMON/nordre/nordre
      COMMON/npi/npi
      COMMON/nrk/nrk
      COMMON/nx/nx
      COMMON/rlx/rlx, xor
      COMMON/visu/mxv
      COMMON/xk/xk
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, irfx, is, k, nxd, nxf
c
c---- Reels
      REAL cflmax, dxm, dxmax, dxmin, dxr, pi, ratio, xlam, xv
c----------------------------------------------------------------------
c
      OPEN(UNIT=1, FILE='Data_Num')
      REWIND(1)
c
      READ(1, *) nlpf
      READ(1, *) nrk
      READ(1, *) nx, rlx, xor
      READ(1, *) iraf
      READ(1, *) nxd, nxf, irfx
      READ(1, *) cfl
      READ(1, *) tmax
      READ(1, *) ktmax
      READ(1, *) nhist, grafx
      READ(1, *) freq
      READ(1, *) xv
      READ(1, *) eps_2
      READ(1, *) fm
      READ(1, *) b
      READ(1, *) L_slab
      READ(1, *) slab_pos
      READ(1, *) icas
      READ(1, *) isort
c     
      CLOSE(1)
c
      pi = ACOS(-1.0)
c
c---- Ordre d'approximation en espace 
c
      nordre = ndl - 1
c
c---- nlpf = 2 ou 4 :  schemas saute-mouton ordre 2 ou 4
c---- nrk  = 0 : schema saute-mouton 
c     nrk >= 1 : schema Runge-Kutta
c
      IF (nrk .EQ. 0)THEN 
         IF (nlpf .EQ. 2)
     &      WRITE(6, *) ' Schema saute-mouton ordre 2'
c
         IF (nlpf .EQ. 4)
     &      WRITE(6, *) ' Schema saute-mouton ordre 4'
c
         IF ((nlpf .NE. 2) .AND. (nlpf .NE. 4)) THEN
            WRITE(6, *) ' nrk=0 ===> nlpf=2 ou 4!'
            WRITE(6, *) ' Dans Data_Num :  mettre nlpf=2 ou 4',
     &                  ' ou nrk=1..4)'
c
            STOP
         ENDIF
      ELSE
         WRITE(6, *) ' Schema en temps Runge-Kutta ', nrk
         WRITE(6, *) ' '
c
         nlpf = 0
c
         DO i=1,nrk
            ark(i) = 1.0/(nrk - i + 1)
         ENDDO
c
         WRITE(6, *) ' Coeff Rkk : ', (ark(i), i=1,nrk)
      ENDIF
c
c---- isort doit etre non nul
c
      IF (isort .EQ. 0) THEN
         WRITE(6, *) ' '
         WRITE(6, *) ' Erreur : isort doit etre # 0'
c   
         STOP
      ENDIF
c
c---- Construction de la grille
c
c     Maillage de [xor,xor+rlx] selon iraf
c     =0: dx uniforme
c     =1: deforme
c     =2: rafinne
c
      coor(1)  = xor
      coor(nx) = xor + rlx
      dxm = rlx/(nx - 1)
c
c---- Grille uniforme
c
      IF (iraf .LE. 1) THEN
         DO i=2,nx-1
            coor(i) = coor(i-1) + dxm 
         ENDDO
c
c----    Grille avec deux pas d'espace
c
         IF (iraf .EQ. 1) THEN
            DO i=2,nx-1,2
               coor(i) = coor(i) + dxm/10.0
            ENDDO
c
            DO i=3,nx-1,2
               coor(i) = coor(i) - dxm/10.0
            ENDDO
         ENDIF
      ELSE
c
c----    Grille uniforme raffinee localement
c
         dxr = dxm/irfx
         is = 0
c
         DO i=1,nxd
            is = is + 1
            coor(is) = (i - 1)*dxm + xor 
         ENDDO
c
         DO i=nxd,nxf-1
            DO k=1,irfx
               is = is + 1
               coor(is) = (i - 1)*dxm + xor + k*dxr
            ENDDO
         ENDDO
c
         DO i=nxf+1,nx
            is = is + 1
            coor(is) =  (i - 1)*dxm + xor 
         ENDDO
c
         nx = is
      ENDIF
c
      WRITE(6, *) ' '
      WRITE(6, *) ' Nombre de points : ', nx
c
c---- On verifie que nx est bien inferieur au max defini dans param.h
c
      IF (nx .GE. nxmax) THEN
        WRITE(6, *) ' '
        WRITE(6, *) ' Erreur : nx > nxmax de param.h'
        WRITE(6, *) ' '
        WRITE(6, *) ' Nx =', nx, ' et NxMax = ', nxmax
        WRITE(6, *) ' '
        WRITE(6, *) ' Changer les dimensions ou modifier param.h'
        WRITE(6, *) '         et recompiler'
        WRITE(6, *) ' '
c
        STOP
      ENDIF
c      
c---- On place le point de visu dans la grille 
c
      mxv = 0
c
      DO i=1,nx
         IF ((xv .GE. coor(i)) .AND. (xv .LT. coor(i+1))) mxv = i
      ENDDO
c      
      IF ((mxv .EQ. 0) .OR. (mxv .GT. nx)) THEN
         WRITE(6, *) ' '
         WRITE(6, *) ' Attention : point de visu hors de la grille'
         WRITE(6, *) ' '
         WRITE(6, *) ' Corriger xv dans Data_Num'
         WRITE(6, *) ' '
c
         STOP
      ENDIF      
c
c---- Pas d'espace local et min
c
      dxmin = 1.0e+12
      dxmax =-1.0e+12
c
      DO i=1,nx-1
         dxi(i) = coor(i+1) - coor(i)
         dxmin = MIN(dxmin, dxi(i))
         dxmax = MAX(dxmax, dxi(i))
      ENDDO
c         
      ratio = dxmax/dxmin
c
c---- La longueur de dxi(0) sert a eviter un 'nan' dans les flux 
c
      dxi(0) = dxi(1)
c
      OPEN(UNIT=2, FILE='Mesh_dx')
      OPEN(UNIT=3, FILE='Mesh_x')
      REWIND(2)
      REWIND(3)
c
      DO i=1,nx-1
         WRITE(2, '(i4, 1x, e12.5)') i, dxi(i)
      ENDDO
c
      DO i=1,nx
         WRITE(3, '(i4, 1x, f12.5)') i, coor(i)
      ENDDO
c
      CLOSE(2)
      CLOSE(3)
c
      WRITE(6, *) ' ' 
      WRITE(6, *) ' Dx min = ', dxmin, ' et ratio Dxmax/Dxmin = ', ratio
c
c---- Donnees provisoires du CFL
c
c---- CFL fonction du schema en temps et de l'ordre k :a etudier
c     A priori CFL(RK4) > CFL(nlpf=4) : a verifier
c
      IF (nlpf .EQ. 2) THEN
cccc     IF (nordre .EQ. 0) cflmax = 1.0
cccc     IF (nordre .EQ. 1) cflmax = 1.0/2.0
cccc     IF (nordre .EQ. 2) cflmax = 1.0/5.0
cccc     IF (nordre .EQ. 3) cflmax = 1.0/8.0
cccc     IF (nordre .EQ. 4) cflmax = 1.0/10.0
cccc     IF (nordre .EQ. 5) cflmax = 5.0/100.0
c
         IF (nordre .EQ. 0) cflmax = 1.00
         IF (nordre .EQ. 1) cflmax = 0.50
         IF (nordre .EQ. 2) cflmax = 0.24
         IF (nordre .EQ. 3) cflmax = 0.15
         IF (nordre .EQ. 4) cflmax = 0.10
         IF (nordre .EQ. 5) cflmax = 0.07
      ENDIF
c
      IF (nlpf .EQ. 4) THEN
cccc     IF (nordre .EQ. 0) cflmax = 2.0
cccc     IF (nordre .EQ. 1) cflmax = 0.6
cccc     IF (nordre .EQ. 2) cflmax = 1.0/4.0
cccc     IF (nordre .EQ. 3) cflmax = 1.0/6.0
cccc     IF (nordre .EQ. 4) cflmax = 1.0/10.0
cccc     IF (nordre .EQ. 5) cflmax = 1.0/20.0
c
         IF (nordre .EQ. 0) cflmax = 1.80
         IF (nordre .EQ. 1) cflmax = 1.42
         IF (nordre .EQ. 2) cflmax = 0.70
         IF (nordre .EQ. 3) cflmax = 0.42
         IF (nordre .EQ. 4) cflmax = 0.28
         IF (nordre .EQ. 5) cflmax = 0.20
      ENDIF
c
      IF (nrk .EQ. 4) THEN
cccc     IF (nordre .EQ. 0) cflmax = 2.0
cccc     IF (nordre .EQ. 1) cflmax = 1.0
cccc     IF (nordre .EQ. 2) cflmax = 0.7
cccc     IF (nordre .EQ. 3) cflmax = 2.0/5.0
cccc     IF (nordre .EQ. 4) cflmax = 1.0/5.0
cccc     IF (nordre .EQ. 5) cflmax = 1.0/10.0
c
         IF (nordre .EQ. 0) cflmax = 1.80
         IF (nordre .EQ. 1) cflmax = 1.41
         IF (nordre .EQ. 2) cflmax = 0.70
         IF (nordre .EQ. 3) cflmax = 0.42
         IF (nordre .EQ. 4) cflmax = 0.28
         IF (nordre .EQ. 5) cflmax = 0.20
      ENDIF
c
      cfl = MIN(cfl, cflmax)
c
      WRITE(6, *) ' CFL = ', cfl
c
c---- Calcul du pas de temps
c
      dt = cfl*dxmin
c
      IF (nrk .GT. 0) dt = 0.5*dt
c
      WRITE(6, *) ' '
      WRITE(6, *) ' Dt = ', dt, ' metres <=> ', dt/0.3, ' nsec'
c
c---- Vitesse dans le vide  c0 (==> vit0) et impulsion omega 
c     (ici xk = omega/c0 par redimensionnement avec tau = c0*t)
c
      xk = 2.0*pi*freq/vit0
      xlam = vit0/freq

      IF (icas .EQ. 6) THEN
            xv=slab_pos+L_slab+2*dxi(1)
      ENDIF
c
c---- Les cas tests 
c
c---- Cas test 1--3 : propagation d'un pulse 1-D 
c                     dans un milieu heterogene
c
c     =1 : fonction gaussienne
c     =2 : fonction marche
c     =3 : fonction triangle
c
c---- Cas test 4 : propagation du mode fondamental
c
      IF (icas .EQ. 4) THEN
         WRITE(6, *) ' '
         WRITE(6, *) ' Propagation du mode 1' 
c
         freq  = 0.5*vit0
         b = 0.
         xlam  = vit0/freq
         xk    = 2.0*pi*freq/vit0
         eps_2 = 1.0
      ENDIF
c
c---- Cas test 5: courant engendre par un dipole dans le vide
c
      IF (icas .EQ. 5)THEN
c
         WRITE(6, *) ' '
         WRITE(6, *) ' Courant dipolaire centre et // a Oz'
c
         eps_2 = 1.0
         b = 0.
      ENDIF
c
      WRITE(6, *) ' '
      WRITE(6, *) ' Frequence incidente : ', freq
      WRITE(6, *) ' Longueur d''onde : ', xlam
      WRITE(6, *) ' '
      WRITE(6, *) ' Temps maximal : ', tmax
      WRITE(6, *) ' Nbre iterations maximales : ', ktmax
c
c---- Calcul des tableaux npi pour calcul de la solution
c
      npi(0)  = 0
      npi(nx) = 0
c
      DO i=1,nx-1
         npi(i) = 1
      ENDDO
c
c----------------------------------------------------------------------
      RETURN
      END

c----------------------------------------------------------------------
