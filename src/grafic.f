c----------------------------------------------------------------------
      SUBROUTINE GRAFIC(xx, yh, yx, ymin, ymax, n, tt)
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER n
c
c---- Reels
      REAL tt
      REAL xx(n), yh(n), yx(n)
c
c---- Tableaux et variables locaux
c
c---- Reels
      REAL xmax, xmin, xmil, ymax, ymin, ymil
c
c---- Caracteres
      CHARACTER*4 cdev
      CHARACTER*60 ctit1
c
      DATA cdev/'xwin'/
c----------------------------------------------------------------------
c
      xmin = xx(1)
      xmax = xx(n)
      xmil = 0.5*(xmax-xmin)
      ymil = 0.5*(ymax-ymin)
c

c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
