c----------------------------------------------------------------------
c                  Matrices et parametres schema P4
c                           Base Lagrange
c----------------------------------------------------------------------
c     Nombre de degres de liberte element : ndl
c     Nombre de degres de liberte face    : ndf
c----------------------------------------------------------------------
      INTEGER ndf, ndl
      PARAMETER (ndl = 5, ndf = 1)
c----------------------------------------------------------------------
c     Matrice de masse multipliee par cmr pour simplifier
c     A diviser par cmr
c
      REAL cmr
      REAL mass(ndl,ndl)
c
      DATA mass/ 
     &     292,  296, -174,   56,  -29,	
     &     296, 1792, -384,  256,   56,	
     &    -174, -384, 1872, -384, -174,	
     &      56,  256, -384, 1792,  296,
     &     -29,   56, -174,  296,  292/ 
c
      DATA cmr/5670/
c----------------------------------------------------------------------
c     Matrice de masse inverse multipliee par cvr pour simplifier
c     A diviser par cvr
c
      REAL cvr
      REAL amat(ndl,ndl)
c
      DATA amat/ 
     &     81920, -12416,  6144,  -2176,  16384,
     &    -12416,  12845,  1104,   -851,  -2176,
     &      6144,   1104, 11520,   1104,   6144,
     &     -2176,   -851,  1104,  12845, -12416,
     &     16384,  -2176,  6144, -12416,  81920/ 
c
      DATA cvr/3276.8/
c----------------------------------------------------------------------
c     Matrice des Phi_j*Dx(Phi_k) (rangement en colonne)
c     A diviser par cgr
c
      REAL cgr
      REAL rdx(ndl,ndl)
c
      DATA rdx/
     &     -945,  1472,  -804,   384, -107,
     &    -1472,     0,  2112, -1024,  384,	
     &      804, -2112,     0,  2112, -804,
     &     -384,  1024, -2112,     0, 1472,
     &      107,  -384,   804, -1472,  945/
c
      DATA cgr/1890/
c----------------------------------------------------------------------
c     Matrices de bords 
c     A diviser par cms
c
      REAL ms1(ndl,ndl), ms2(ndl,ndl), ms3(ndl,ndl), ms4(ndl,ndl)
c
      REAL cms
      DATA cms/1/
c
      DATA ms1/ 
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     1, 0, 0, 0, 0/
c
      DATA ms2/ 
     &     1, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0/
c
      DATA ms3/ 
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 1/
c
      DATA ms4/ 
     &     0, 0, 0, 0, 1,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0,
     &     0, 0, 0, 0, 0/
c----------------------------------------------------------------------
