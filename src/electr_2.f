c----------------------------------------------------------------------
      SUBROUTINE ELECTR_2(dt, ts, ua, un)
c----------------------------------------------------------------------
c     Mise a jour du champ electrique, schema saute-mouton d'ordre 2
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER i_d, icas, nx
      INTEGER npi(0:nxmax)
c
c---- Reels
      REAL dt, ts
      REAL dxi(0:nxmax), epsil_r(nxmax), flux(ndl,nxmax),
     &     ua(2,ndl,0:nx), un(2,ndl,0:nx)
c
c---- Fonction externe 
c
      REAL JCT,J_SIN
      EXTERNAL JCT,J_SIN
c
      COMMON/i_d/i_d
      COMMON/dxi/dxi
      COMMON/epsil/epsil_r
      COMMON/flux/flux
      COMMON/icas/icas
      COMMON/nx/nx
      COMMON/npi/npi
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, j, k
c
c---- Reels
      REAL dtf
      REAL fld(ndl), flg(ndl), flu(ndl)
c----------------------------------------------------------------------
c
      DO k=1,ndl
         DO i=1,nx
            flux(k,i) = 0.0
         ENDDO
      ENDDO
c
c---- Integrale sur les elements 
c     
      DO i=1,nx-1 
         DO k=1,ndl
            flu(k) = 0.0
c
            DO j=1,ndl
               flu(k) = flu(k) + ua(1,j,i)*rdx(k,j)
            ENDDO
c
            flux(k,i) = flux(k,i) - flu(k)/cgr
         ENDDO
      ENDDO
c
c---- Termes d'interface [i:i+1]
c
      DO i=1,nx-1
         DO k=1,ndl
            fld(k) = 0.0
            flg(k) = 0.0

            DO j=1,ndl
               fld(k) = fld(k) + ua(1,j,i)*ms3(k,j) +
     &                           ua(1,j,i+1)*ms4(k,j)
               flg(k) = flg(k) + ua(1,j,i-1)*ms1(k,j) +
     &                           ua(1,j,i)*ms2(k,j)
            ENDDO

            flux(k,i) = flux(k,i) + 0.5*fld(k)*npi(i+1)/cms
            flux(k,i) = flux(k,i) - 0.5*flg(k)*npi(i-1)/cms
         ENDDO
      ENDDO
c
c---- Conditions aux bords 
c
      CALL E_BORDS(ua)
c
c---- Multiplication par la matrice de masse inverse
c
      DO i=1,nx-1 
         dtf = dt/(epsil_r(i)*dxi(i)*cvr)
c
         DO k=1,ndl
            flu(k) = 0.0
c
            DO j=1,ndl
               flu(k) = flu(k) + amat(k,j)*flux(j,i)
            ENDDO
c
            un(2,k,i) = ua(2,k,i) + dtf*flu(k)
         ENDDO
      ENDDO
c
c---- Courant source d'un dipole 1-D (icas=5)
c
      IF (icas .EQ. 5) THEN
         i = i_d
c
         DO k=1,ndl
            un(2,k,i)   = un(2,k,i)   - dt*JCT(ts)/dxi(i)
            un(2,k,i-1) = un(2,k,i-1) - dt*JCT(ts)/dxi(i-1)
         ENDDO
      ENDIF

      IF (icas .EQ. 6) THEN
         i = i_d
c
         DO k=1,ndl
            un(2,k,i)   = un(2,k,i)   - dt*J_SIN(ts)/dxi(i)
            un(2,k,i-1) = un(2,k,i-1) - dt*J_SIN(ts)/dxi(i-1)
         ENDDO
      ENDIF
c
c---- Mise a jour
c
      DO k=1,ndl
         DO i=1,nx
            ua(2,k,i) = un(2,k,i)
         ENDDO
      ENDDO
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
