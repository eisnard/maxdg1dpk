c----------------------------------------------------------------------
c                  Matrices et parametres schema P0
c                           Base Lagrange
c----------------------------------------------------------------------
c     Nombre de degres de liberte element : ndl
c     Nombre de degres de liberte face    : ndf
c----------------------------------------------------------------------
      INTEGER ndf, ndl
      PARAMETER (ndl = 1, ndf = 1)
c----------------------------------------------------------------------
c     Matrice de masse multipliee par cmr pour simplifier
c     A diviser par cmr
c
      REAL cmr
      REAL mass(ndl,ndl)
c
      DATA mass/1/ 
      DATA cmr/1/
c----------------------------------------------------------------------
c     Matrice de masse inverse multipliee par cvr pour simplifier
c     A diviser par cvr
c
      REAL cvr
      REAL amat(ndl,ndl)
c
      DATA amat/1/ 
      DATA cvr/1/
c----------------------------------------------------------------------
c     Matrice des Phi_j*Grad(Phi_k) (rangement en colonne)
c     A diviser par cgr
c
      REAL cgr
      REAL rdx(ndl,ndl)
c
      DATA rdx/0/
c
      DATA cgr/1/
c----------------------------------------------------------------------
c     Matrices de bords 
c     A diviser par cms
c
      REAL ms1(ndl,ndl), ms2(ndl,ndl), ms3(ndl,ndl), ms4(ndl,ndl)
c
      DATA ms1/1/
      DATA ms2/1/
      DATA ms3/1/
      DATA ms4/1/
c----------------------------------------------------------------------
