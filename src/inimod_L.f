c----------------------------------------------------------------------
      SUBROUTINE INIMOD_L(dt)
c----------------------------------------------------------------------
c     Initialisation mode propre, base de Lagrange
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER imet, nx
      INTEGER mfx(2)
c
c---- Reels
      REAL dt, xk
      REAL coor(nxmax), ua(2,ndl,0:nxmax), x_L(ndl)
c
      COMMON/coor/coor
      COMMON/imf/mfx
      COMMON/imet/imet
      COMMON/nx/nx
      COMMON/ua/ua
      COMMON/xk/xk
      COMMON/x_L/x_L
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, k
c
c---- Reels
      REAL pi, sint, xi
c----------------------------------------------------------------------
c
c---- Conditions aux bords : imet=1 ==> tous les mf=1
c---- Aucune face absorbante (ifk=0 dans INIT)
c
      imet = 1
c
      DO i=1,2
         mfx(i) = 1
      ENDDO
c
      pi = ACOS(-1.0)
      sint = SIN(0.5*xk*dt)
c
      DO i=1,nx-1
         DO k=1,ndl
            xi = (coor(i+1) - coor(i))*x_L(k) + coor(i)
            xi = pi*xi
c
            ua(1,k,i) = COS(xi)*sint
            ua(2,k,i) = SIN(xi)
         ENDDO
      ENDDO
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
