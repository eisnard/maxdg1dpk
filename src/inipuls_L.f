c----------------------------------------------------------------------
      SUBROUTINE INIPULS_L
c----------------------------------------------------------------------
c     Propagation d'un pulse gaussien dans un milieu heterogene 
c     Base de Lagrange
c
c     Vide a gauche de l'interface et materiau a droite 
c     Le pulse est centre dans le vide a t=0
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER icas, nx
      INTEGER ifx(2)
c
c---- Reels
      REAL eps_2, expo, rlx,  x_a, x_b, x_g, xm1, xm2, xor
      REAL coor(nxmax), epsil_r(nxmax), ua(2,ndl,0:nxmax), x_L(ndl)
c
      REAL E1, E2, E3, H1, H2, H3
      EXTERNAL E1, E2, E3, H1, H2, H3
c
      COMMON/coor/coor
      COMMON/eps_2/eps_2
      COMMON/epsil/epsil_r
      COMMON/gauss/expo, x_g
      COMMON/icas/icas
      COMMON/iinf/ifx
      COMMON/mater/xm1, xm2
      COMMON/rlx/rlx, xor
      COMMON/nx/nx
      COMMON/ua/ua
      COMMON/x_ab/x_a, x_b
      COMMON/x_L/x_L
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, k
c
c---- Reels
      REAL xi
c----------------------------------------------------------------------
c
c---- Conditions aux bords : imet=0 et tout le bord est absorbant
c     Aucune face metallique (imet et mfk = 0 dans INIT)
c
      DO i=1,2
         ifx(i) = 1
      ENDDO
c
c---- Position du pulse : dans le  vide 
c
      xm1 = xor
      xm2 = xor + rlx
      x_g = xor + 0.25*rlx
c
      IF (eps_2 .GT. 1) THEN
         xm1 = xor + 0.5*rlx
         xm2 = 1.0e+06
         x_g = xor + 0.25*rlx
      ENDIF
c
c---- Materiau heterogene 
c
!       DO i=1,nx-1
!          xi = 0.5*(coor(i) + coor(i+1))
! c
!          IF (xi .GT. xm1) epsil_r(i) = eps_2
!       ENDDO
c
      IF (icas .EQ. 1) THEN
         expo = 10.0
c
         DO i=1,nx-1
            DO k=1,ndl
               xi = (coor(i+1) - coor(i))*x_L(k) + coor(i)         
               ua(1,k,i) = H1(xi)
               ua(2,k,i) = E1(xi)
            ENDDO
         ENDDO
      ENDIF
c
c---- La marche occupe un 1/6 de l´intervalle 
c     et se trouve a gauche (dans le vide si mulieu heterogene)
c
      IF (icas .EQ. 2) THEN
         x_a = xor + rlx/6.0
         x_b = x_a + rlx/6.0
c
         DO i=1,nx-1
            DO k=1,ndl
               xi = (coor(i+1) - coor(i))*x_L(k) + coor(i)         
               ua(1,k,i) = H2(xi)
               ua(2,k,i) = E2(xi)
            ENDDO
         ENDDO
      ENDIF
c
c---- Le triangle occupe un 1/6 de l´intervalle 
c     et se trouve a gauche (dans le vide si millieu heterogene)
c
      IF (icas .EQ. 3) THEN
         x_a = xor + rlx/6.0
         x_b = x_a + rlx/6.0
c
         DO i=1,nx-1
            DO k=1,ndl
               xi = (coor(i+1) - coor(i))*x_L(k) + coor(i)
c       
               ua(1,k,i) = H3(xi)
               ua(2,k,i) = E3(xi)
            ENDDO
         ENDDO
      ENDIF
c
      OPEN(UNIT=30, FILE='H0_x')
      OPEN(UNIT=31, FILE='E0_x')
      REWIND(30)
      REWIND(31)
c
      DO i=1,nx-1
         xi = 0.5*(coor(i) + coor(i+1))
c
         WRITE(30, *) xi, ua(1,1,i)
         WRITE(31, *) xi, ua(2,1,i)
      ENDDO
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
