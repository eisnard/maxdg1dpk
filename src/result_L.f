c----------------------------------------------------------------------
      SUBROUTINE RESULT_L(dt, t_e)
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER icas, ifin, nhist
c
c---- Reels
      REAL dt, t_e
c
      COMMON/icas/icas
      COMMON/ifin/ifin
      COMMON/nhist/nhist
c----------------------------------------------------------------------
c
      IF ((nhist .NE. 1) .AND. (ifin .NE. 1)) RETURN
c
      REWIND(80)
c
      IF (icas .LE. 5) REWIND(81)
c
      IF (icas .EQ. 1) CALL SOLPULS1_L(t_e)
      IF (icas .EQ. 2) CALL SOLPULS2_L(t_e)
      IF (icas .EQ. 3) CALL SOLPULS3_L(t_e)
      IF (icas .EQ. 4) CALL SOLMOD_L(dt, t_e)
      IF (icas .EQ. 5) CALL SOLDIPO_L(dt, t_e)
      IF (icas .EQ. 6) CALL SAVE_6TH_CASE(t_e)
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
