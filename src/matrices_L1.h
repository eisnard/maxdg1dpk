c----------------------------------------------------------------------
c                  Matrices et parametres schema P1
c                           Base Lagrange
c----------------------------------------------------------------------
c     Nombre de degres de liberte element : ndl
c     Nombre de degres de liberte face    : ndf
c----------------------------------------------------------------------
      INTEGER ndf, ndl
      PARAMETER (ndl = 2, ndf = 1)
c----------------------------------------------------------------------
c     Matrice de masse multipliee par cmr pour simplifier
c     A diviser par cmr
c
      REAL cmr
      REAL mass(ndl,ndl)
c
      DATA mass/ 
     &     2, 1, 
     &     1, 2/
c
      DATA cmr/6/
c----------------------------------------------------------------------
c     Matrice de masse inverse multipliee par cvr pour simplifier
c     A diviser par cvr
c
      REAL cvr
      REAL amat(ndl,ndl)
c
      DATA amat/ 
     &     4, -2,
     &    -2,  4/
c
      DATA cvr/1/
c----------------------------------------------------------------------
c     Matrice des Phi_j*Grad(Phi_k) (rangement en colonne)
c     A diviser par cgr 
c
      REAL cgr
      REAL rdx(ndl,ndl)
c
      DATA rdx/
     &    -1, 1,
     &    -1, 1/
c
      DATA cgr/2/
c----------------------------------------------------------------------
c     Matrices de bords 
c     A diviser par cms
c
      REAL ms1(ndl,ndl), ms2(ndl,ndl), ms3(ndl,ndl), ms4(ndl,ndl)
c
      REAL cms
      DATA cms/1/
c
      DATA ms1/ 
     &     0, 0,
     &     1, 0/
c
      DATA ms2/ 
     &     1, 0,
     &     0, 0/
c
      DATA ms3/ 
     &     0, 0,
     &     0, 1/
c
      DATA ms4/ 
     &     0, 1,
     &     0, 0/
c----------------------------------------------------------------------
