c----------------------------------------------------------------------
      SUBROUTINE RKKITER(dt,temps,ts,ua,un)
c----------------------------------------------------------------------
c     Mise a jour des champs electrique et magnetique, schema RKk
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER i_d, icas, nrk, nx 
      INTEGER ifx(2), mfx(2), npi(0:nxmax)
c
c---- Reels
      REAL dt, temps, ts
      REAL ark(6), dxi(0:nxmax), epsil_r(nxmax), 
     &     ua(2,ndl,0:nx), un(2,ndl,0:nx)
c
c---- Fonction externe 
c
      REAL JCT,J_SIN
      EXTERNAL JCT,J_SIN
c
      COMMON/ark/ark
      COMMON/dxi/dxi
      COMMON/i_d/i_d
      COMMON/epsil/epsil_r
      COMMON/icas/icas
      COMMON/iinf/ifx
      COMMON/imf/mfx
      COMMON/nx/nx
      COMMON/npi/npi
      COMMON/nrk/nrk
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, irk, j, k, kx
c
c---- Reels
      REAL dtk, z1, zn
      REAL flg_e(ndl), fld_e(ndl), flg_h(ndl), fld_h(ndl), 
     &     flu(2,ndl), flu_e(ndl,nxmax), flu_h(ndl, nxmax)
c----------------------------------------------------------------------
c
c---- On copie ua dans un
c
      DO k=1,ndl
         DO i=1,nx
            un(1,k,i) = ua(1,k,i)
            un(2,k,i) = ua(2,k,i)
         ENDDO
      ENDDO
c
      kx = nx - 1
c
      z1 = SQRT(epsil_r(1))
      zn = SQRT(epsil_r(nx-1))
c
c---- Calcul energie 
c
      CALL ENERGY(temps, un, un)
c
c---- Boucle Runge-Kutta
c
      DO irk=1,nrk
c
c----    Calcul du kieme itere de E,H
c
         DO k=1,ndl
            DO i=1,nx-1
               flu_e(k,i) = 0.0
               flu_h(k,i) = 0.0
            ENDDO
         ENDDO
c
c----    Integrale sur les elements 
c     
         DO i=1,nx-1 
            DO k=1,ndl
               flu(1,k) = 0.0
               flu(2,k) = 0.0
c
               DO j=1,ndl
                  flu(1,k) = flu(1,k) + ua(1,j,i)*rdx(k,j)
                  flu(2,k) = flu(2,k) + ua(2,j,i)*rdx(k,j)
               ENDDO
c
               flu_e(k,i) = flu_e(k,i) - flu(1,k)/cgr
               flu_h(k,i) = flu_h(k,i) - flu(2,k)/cgr
            ENDDO
         ENDDO
c
c----    Termes d'interface [i:i+1]
c
         DO i=1,nx-1
            DO k=1,ndl
               fld_e(k)  = 0.0
               flg_e(k)  = 0.0
               fld_h(k)  = 0.0
               flg_h(k)  = 0.0
c
               DO j=1,ndl
                  fld_e(k) = fld_e(k) + ua(1,j,i)*ms3(k,j) +
     &                                  ua(1,j,i+1)*ms4(k,j)
                  flg_e(k) = flg_e(k) + ua(1,j,i-1)*ms1(k,j) +
     &                                  ua(1,j,i)*ms2(k,j)
c
                  fld_h(k) = fld_h(k) + ua(2,j,i)*ms3(k,j) +
     &                                  ua(2,j,i+1)*ms4(k,j)
                  flg_h(k) = flg_h(k) + ua(2,j,i-1)*ms1(k,j) +
     &                                  ua(2,j,i)*ms2(k,j)
               ENDDO
c
               flu_e(k,i) = flu_e(k,i) + 0.5*fld_e(k)*npi(i+1)/cms
     &                                 - 0.5*flg_e(k)*npi(i-1)/cms
c
               flu_h(k,i) = flu_h(k,i) + 0.5*fld_h(k)*npi(i+1)/cms 
     &                                 - 0.5*flg_h(k)*npi(i-1)/cms 
            ENDDO
         ENDDO
c
c----    Conditions aux bords 
c
c----    Bord metallique: uniquement pour E
c
         DO k=1,ndl
            flg_e(k) = 0.0
            fld_e(k) = 0.0
c
            DO j=1,ndl
               flg_e(k) = flg_e(k) + ua(1,j,1 )*ms2(k,j)
               fld_e(k) = fld_e(k) + ua(1,j,kx)*ms3(k,j)
            ENDDO

            flu_e(k,1)  = flu_e(k,1)  - flg_e(k)*mfx(1)/cms
            flu_e(k,kx) = flu_e(k,kx) + fld_e(k)*mfx(2)/cms
         ENDDO
c
c----    Bord artificiel (CLA)
c
         DO k=1,ndl
            flg_e(k) = 0.0
            fld_e(k) = 0.0
            flg_h(k) = 0.0
            fld_h(k) = 0.0

            DO j=1,ndl
               flg_e(k) = flg_e(k) + 0.5*
     &                    ( ua(1,j,1)  + z1*ua(2,j,1 ))*ms2(k,j)
               fld_e(k) = fld_e(k) + 0.5*
     &                    (-ua(1,j,kx) + zn*ua(2,j,kx))*ms3(k,j)

               flg_h(k) = flg_h(k) + 0.5* 
     &                    (ua(2,j,1)  + ua(1,j,1 )/z1)*ms2(k,j)
               fld_h(k) = fld_h(k) + 0.5*  
     &                    (ua(2,j,kx) - ua(1,j,kx)/zn)*ms3(k,j)
            ENDDO
c
            flu_e(k,1)   = flu_e(k,1)  - flg_e(k)*ifx(1)/cms
            flu_e(k,kx)  = flu_e(k,kx) - fld_e(k)*ifx(2)/cms
            flu_h(k,1)   = flu_h(k,1)  - flg_h(k)*ifx(1)/cms
            flu_h(k,kx)  = flu_h(k,kx) + fld_h(k)*ifx(2)/cms
         ENDDO
c
c----    Multiplication par la matrice de masse inverse
c
         DO i=1,nx-1 
            dtk = ark(irk)*dt/(dxi(i)*cvr)
c
            DO k=1,ndl
               flu(1,k) = 0.0
               flu(2,k) = 0.0
c
               DO j=1,ndl
                  flu(1,k) = flu(1,k) + amat(k,j)*flu_h(j,i)
                  flu(2,k) = flu(2,k) + amat(k,j)*flu_e(j,i)
               ENDDO
c
               ua(1,k,i) = un(1,k,i) + dtk*flu(1,k)
               ua(2,k,i) = un(2,k,i) + dtk*flu(2,k)/epsil_r(i) 
            ENDDO
         ENDDO
c
c----    Fin boucle Runge-Kutta
c
      ENDDO
c
c---- Courant source d'un dipole 1-D (icas=5)
c
      IF (icas .EQ. 5) THEN
         i = i_d
c
         Do k=1,ndl
            ua(2,k,i)   = ua(2,k,i)   - dt*JCT(ts)/dxi(i)
            ua(2,k,i-1) = ua(2,k,i-1) - dt*JCT(ts)/dxi(i-1)
         ENDDO
      ENDIF

      IF (icas .EQ. 6) THEN
         i = i_d
c
         Do k=1,ndl
            ua(2,k,i)   = ua(2,k,i)   - dt*J_SIN(ts)/dxi(i)
            ua(2,k,i-1) = ua(2,k,i-1) - dt*J_SIN(ts)/dxi(i-1)
         ENDDO
      ENDIF
c
c---- Mise a jour
c
      DO k=1,ndl
         DO i=1,nx
            un(1,k,i) = ua(1,k,i)
            un(2,k,i) = ua(2,k,i)
         ENDDO
      ENDDO
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
