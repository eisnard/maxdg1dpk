c----------------------------------------------------------------------
      SUBROUTINE INIDIPO
c----------------------------------------------------------------------
c     Dipole 1D (en x) d'axe // a Oz place au centre 
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER i_d, nx
      INTEGER ifx(2)
c
c---- Reels
      REAL a_d, t0_d
      REAL coor(nxmax), ua(2,ndl,0:nxmax)
c
      COMMON/coor/coor
      COMMON/dipol/a_d, t0_d
      COMMON/i_d/i_d
      COMMON/iinf/ifx
      COMMON/nx/nx
      COMMON/ua/ua
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, k
c
c---- Reels 
      REAL x0
c----------------------------------------------------------------------
c
c---- Conditions aux bords : imet=0 et tout le bord est absorbant
c---- Aucune face metallique (imet et mfk=0 dans INIT)

      DO i=1,2
         ifx(i) = 1
      ENDDO
c
c---- Cas test 1-D (x) 
c---- Parametres du cas test: dipole au centre du domaine
c
      a_d  = 10.0
      t0_d = 0.6
      i_d  = INT(nx/10)
c
      x0 = 0.5*(coor(i_d) + coor(i_d+1))
c
      DO i=1,nx-1
         DO k=1,ndl
            ua(1,k,i) = 0.0
            ua(2,k,i) = 0.0
         ENDDO
      ENDDO
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
