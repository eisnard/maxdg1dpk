c----------------------------------------------------------------------
      SUBROUTINE PULSE2(solx, temps, xi)
c---------------------------------------------------------------------- 
c     Solution exacte du pulse 1-D TM (x) dans le vide
c     Cas test 2 : impulsion type marche
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
c---- Variables partagees
c
      REAL eps_2, temps, xi, xm1, xm2
      REAL solx(2)
c
      COMMON/eps_2/eps_2
      COMMON/mater/xm1, xm2
c
c---- Variables locales
c
      REAL a1, a3, b1, b2, b3, c1, c12, c121, c13, c1t, c2, c21, 
     &     c211, c212, c23, c232, c233, c3, c31, c32, c323, c2t, c3t, 
     &     d2, E2a, E2r, Ez_x, H2a, H2r, Hy_x, x1, x2, x_a, x_b,
     &     x_c, x_d, y1, y2, y3, y4, y5, y6, z1, z2, z3
c----------------------------------------------------------------------
c
c---- Vitesses et impedances relatives : mu_r = 1 => Zr = c_r
c
      c1  = 1.0 
      c2  = 1.0/SQRT(eps_2)
      c3  = c2
c
      c12 = c1/c2
      c13 = c1/c3
      c21 = c2/c1
      c23 = c2/c3
      c31 = c3/c1
      c32 = c3/c2
c
      c121 = (c1 - c2)/c1
      c211 = (c2 - c1)/c1
      c212 = (c2 - c1)/c2
      c232 = (c2 - c3)/c2
      c233 = (c2 - c3)/c3
      c323 = (c3 - c2)/c3
c
      c1t = c1*temps
      c2t = c2*temps
      c3t = c3*temps
c
      z1 = c1
      z2 = c2
      z3 = c3
c
      x1 = xm1
      x2 = xm2
c
      a1 = (z1 - z2)/(z2 + z1)
      a3 = (z3 - z2)/(z2 + z3)
      b1 = 2.0*z1/(z2 + z1)  
      b2 = 2.0*z2/(z2 + z1)
      b3 = 2.0*z3/(z2 + z3)  
      d2 = 2.0*z2/(z2 + z3)
c
c---- Calcul au point x = xi
c
c---- Zone (I) : x <= x1 - c1*T
c
      IF (xi .LE. (x1 - c1t)) THEN
         y1   = xi - c1t
         y2   = xi + c1t
         Hy_x = H2r(y1,z1) + H2a(y2,z1) 
         Ez_x = E2r(y1,z1) + E2a(y2,z1) 
      ENDIF
c
c---- Zone (II) : x1 - c1*T < x <= x1 
c
      IF ((xi .GT. (x1 - c1t)) .AND. (xi .LE. x1)) THEN
         y1   = xi - c1t
         y2   =-xi - c1t + 2.0*x1 
         Hy_x = H2r(y1,z1) + a1*H2r(y2,z1)
         Ez_x = E2r(y1,z1) - a1*E2r(y2,z1)    

         x_a = x1 -c1t + c12*(x2 - x1)
c
c----    Zone (II-1) : x <= x_a
c
         IF (xi .LE. x_a) THEN
            y3   = c21*xi + c2t + c121*x1
            Hy_x = Hy_x + b2*H2a(y3,z2) 
            Ez_x = Ez_x + b1*E2a(y3,z2) 
         ELSE
c
c----       Zone (II-2) : x > x_a
c
            y3   =-c21*xi - c2t + c211*x1 + 2.0*x2 
            y4   = c31*xi + c3t + c121*c32*x1 + c232*x2
            Hy_x = Hy_x - a3*b2*H2r(y3,z2) + b2*b3*H2a(y4,z3) 
            Ez_x = Ez_x + a3*b1*E2r(y3,z2) + b1*d2*E2a(y4,z3)
         ENDIF
      ENDIF
c
c---- Zone (III) : x1 < x <= Min(x1 + c2*T, x2)
c
      IF ((xi .GT. x1) .AND. (xi .LE. MIN(x1 + c2t, x2))) THEN
         y1   = c12*xi - c1t + c212*x1
         Hy_x = b1*H2r(y1,z1) 
         Ez_x = b2*E2r(y1,z1) 
c
c----    Zone (III-1) : x <= x2 - c2*T 
c
         IF (xi .LE. (x2 - c2t)) THEN
            y2   =-xi + c2t + 2.0*x1 
            y3   = xi + c2t
            Hy_x = Hy_x - a1*H2a(y2,z2) + H2a(y3,z2)
            Ez_x = Ez_x + a1*E2a(y2,z2) + E2a(y3,z2)
         ELSE
c
c----       Zone (III-2) : x > x2 - c2*T
c
            IF (xi .GT. (x2-c2t)) THEN
               x_c = 2.0*x2 - x1 - c2t
               x_d = 2.0*x1 - x2 + c2t
c 
c-----         Zone (III-2-1) :   x <= Min(x_c , x_d)
c
               IF (xi .LE. MIN(x_c, x_d)) THEN
                  y2   =-xi - c2t + 2.0*x2
                  y3   = xi - c2t + 2.0*(x2 - x1) 
                  y4   =-c32*xi + c3t + c232*x2 + 2*c32*x1 
                  y5   =  c32*xi + c3t + c232*x2
                  Hy_x = Hy_x + a3*(-H2r(y2,z2) + a1*H2r(y3,z2)) +  
     &                          b3*(-a1*H2a(y4,z3) + H2a(y5,z3))
                  Ez_x = Ez_x + a3*( E2r(y2,z2) + a1*E2r(y3,z2)) +
     &                          d2*( a1*E2a(y4,z3) + E2a(y5,z3))
               ENDIF
c 
c----          Zone (III-2-2) : x_c < x < x_d
c
               IF ((xi .GT. x_c) .AND. (xi .LE. x_d)) THEN
                  y2   =-c12*xi - c1t + c212*x1 + 2.0*c12*x2 
                  y3   = xi - c2t + 2.0*(x2 - x1) 
                  y4   = xi + c2t + 2.0*(x1 - x2) 
                  y5   =-c32*xi + c3t + c232*x2 + 2*c32*x1 
                  y6   = c32*xi + c3t + c232*x2 
                  Hy_x = Hy_x + a3*(-b1*H2r(y2,z1) + a1*H2r(y3,z2)) +  
     &                          a1*( a3*H2a(y4,z2) - b3*H2a(y5,z3)) + 
     &                          b3*H2a(y6,z3)  
                  Ez_x = Ez_x + a3*( b2*E2r(y2,z1) + a1*E2r(y3,z2)) +  
     &                          a1*( a3*E2a(y4,z2) + d2*E2a(y5,z3)) + 
     &                          d2*E2a(y6,z3)  
               ENDIF
c 
c----          Zone (III-2-3) : x_d < x < x_c
c
               IF ((xi .GT. x_d) .AND. (xi .LE. x_c)) THEN
                  y2   =-xi + c2t + 2.0*x1 
                  y3   =-xi - c2t + 2.0*x2
                  y4   = c32*xi + c3t + c232*x2 
                  Hy_x = Hy_x - a1*H2a(y2,z2) - a3*H2r(y3,z2) + 
     &                          b3*H2a(y4,z3) 
                  Ez_x = Ez_x + a1*E2a(y2,z2) + a3*E2r(y3,z2) +
     &                          d2*E2a(y4,z3) 
               ENDIF
c
c----          Zone (III-2-4) : x > Max(x_c , x_d)
c
               IF (xi .GT. MAX(x_c, x_d)) THEN
                  y2   =-c12*xi - c1t + c212*x1 + 2.0*c12*x2
                  y3   =-xi + c2t + 2.0*x1
                  y4   =  xi + c2t + 2.0*(x1 - x2)
                  y5   = c32*xi + c3t + c232*x2 
                  Hy_x = Hy_x - a3*b1*H2r(y2,z1) - a1*H2a(y3,z2) + 
     &                          a1*a3*H2a(y4,z2) + b3*H2a(y5,z3) 
                  Ez_x = Ez_x + a3*b2*E2r(y2,z1) + a1*E2a(y3,z2) + 
     &                          a1*a3*E2a(y4,z2) + d2*E2a(y5,z3)  
               ENDIF
            ENDIF
         ENDIF
      ENDIF            
c
c---- Zone (IV) :  x1 + c2*T < x <= x2 
c
      IF ((xi .GT. (x1 + c2t)) .AND. (xi .LE. x2)) THEN
         y1   = xi - c2t
         Hy_x = H2r(y1,z2)
         Ez_x = E2r(y1,z2) 
c 
c-----   Zone (IV-1) : x < x2 - c2*T
c
         IF(xi .LE. (x2 - c2t)) THEN
            y2   = xi + c2t
            Hy_x = Hy_x + H2a(y2,z2)  
            Ez_x = Ez_x + E2a(y2,z2)  
         ELSE
c 
c----       Zone (IV-2) : x > x2 - c2*T
c
            y2   =-xi - c2t + 2.0*x2
            y3   = c32*xi + c3t + c232*x2
            Hy_x = Hy_x - a3*H2r(y2,z2) + b3*H2a(y3,z3)  
            Ez_x = Ez_x + a3*E2r(y2,z2) + d2*E2a(y3,z3)
         ENDIF
      ENDIF
c
c---- Zone (V) : x2 < x <= x2 + c3*T
c
      IF ((xi .GT. x2) .AND. (xi .LE. (x2 + c3t))) THEN
         x_b = x2 + c3t + c32*(x1 - x2)

c----    Zone (V-1) : x <= x_b

         IF (xi .LE. x_b) THEN
            y1   = c13*xi - c1t + x1 + c12*(x2 - x1) -c13*x2
            y2   =-c23*xi + c2t + c233*x2 + 2.0*x1
            y3   =-xi + c3t + 2.0*x2 
            y4   = xi + c3t
            Hy_x = b1*d2*H2r(y1,z1) - a1*d2*H2a(y2,z2) +
     &                a3*H2a(y3,z3) + H2a(y4,z3) 
            Ez_x = b2*b3*E2r(y1,z1) + a1*b3*E2a(y2,z2) -
     &                a3*E2a(y3,z3) + E2a(y4,z3) 
         ELSE

c----       Zone (V-2) : x > x_b
c
            y1   = c23*xi - c2t + c323*x2 
            y2   =-xi + c3t + 2.0*x2 
            y4   = xi + c3t
            Hy_x = d2*H2r(y1,z2) + a3*H2a(y2,z3) + H2a(y3,z3)
            Ez_x = b3*E2r(y1,z2) - a3*E2a(y2,z3) + E2a(y3,z3)
         ENDIF
      ENDIF
c
c---- Zone (VI) : x >  x2 + c3*T
c
      IF (xi .GT. (x2 + c1t)) THEN
         y1   = xi - c3t
         y2   = xi + c3t
         Hy_x = H2r(y1,z3) + H2a(y2,z3) 
         Ez_x = E2r(y1,z3) + E2a(y2,z3)  
      ENDIF
c
      solx(1) = 0.5*Hy_x
      solx(2) = 0.5*Ez_x
c
      IF (ABS(solx(1)) .LT. 1.0E-08) solx(1) = 0.0
      IF (ABS(solx(2)) .LT. 1.0e-08) solx(2) = 0.0
c                         
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
c---- Fonctions champs avances et retardes 
c----------------------------------------------------------------------
c----------------------------------------------------------------------
      FUNCTION E2a(x,z)
c----------------------------------------------------------------------
c     Champ electrique avance dans le milieu d'indice Z
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
c---- Variables partagees
c
      REAL E2, E2a, H2, x, z
      EXTERNAL E2, H2
c----------------------------------------------------------------------
c
      E2a = E2(x) + z*H2(x)
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
      FUNCTION E2r(x,z)
c----------------------------------------------------------------------
c     Champ electrique retarde dans le milieu d'indice Z
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
c---- Variables partagees
c
      REAL E2, E2r, H2, x, z
      EXTERNAL E2, H2
c----------------------------------------------------------------------
c
      E2r = E2(x) - z*H2(x)
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
      FUNCTION H2a(x,z)
c----------------------------------------------------------------------
c     Champ magnetique avance dans le milieu d'indice Z
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
c---- Variables partagees
c
      REAL E2, H2, H2a, x, z
      EXTERNAL E2, H2
c----------------------------------------------------------------------
c
      H2a = H2(x) + E2(x)/z
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
      FUNCTION H2r(x,z)
c----------------------------------------------------------------------
c     Champ magnetique retarde dans le milieu d'indice Z
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
c---- Variables partagees
c
      REAL E2, H2, H2r, x, z
      EXTERNAL E2, H2
c----------------------------------------------------------------------
c
      H2r = H2(x) - E2(x)/z
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
c---- Fonctions initiales 
c----------------------------------------------------------------------
      FUNCTION E2(x)
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
c---- Variables partagees
c
      REAL E2, x, x_a, x_b
      COMMON/x_ab/x_a, x_b
c----------------------------------------------------------------------
c
      E2 = 0.0
c
      IF ((x .GE. x_a) .AND. (x .LE. x_b)) E2 = 1.0
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
      FUNCTION H2(x)
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
c---- Variables partagees
c
      REAL H2, x, x_a, x_b
      COMMON/x_ab/x_a, x_b
c----------------------------------------------------------------------
c
      H2 = 0.0
c
      IF ((x .GE. x_a) .AND. (x .LE. x_b)) H2 =-1.0
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
