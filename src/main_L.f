c----------------------------------------------------------------------
      program MAIN_L
c----------------------------------------------------------------------
c---- Schema Galerkin discontinu avec bases de Lagrange
c     et schemas en temps saute-mouton (nlpf=k ordre = k, k=2 ou 4)
c     ou  Rkk (nrk > 0)
c       
c     ua(1:2) ==> champ Ez,Hy au  temps t^n et t^(n+1/2)
c     un(1:2) ==> champ Ez,Hy au  temps t^(n+1) et t^(n+3/2)
c                 et/ou champs intermediaires
c
c---- Visualisation
c
c     si grafx=0 : visu par gnuplot apres les calculs     
c     si grafx=1 : visu par dislin pendantles calculs  
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      integer ba,grafx,icas,i_d,ifin,kt,ktmax,nlpf,nordre,nrk,nx
c
c---- Reels
      REAL cfl,dt,dt_h,err_max,freq,temps,tmax,tsour,L_slab,slab_pos
      REAL xd,xg
      REAL bn(ndl,0:nxmax),dxi(0:nxmax),ua(2,ndl,0:nxmax),un(2,ndl,
     &   0:nxmax)
c
c---- Fonction externe 
c
      REAL JCT,EPS_FUNC,J_SIN
      EXTERNAL JCT,EPS_FUNC,J_SIN


      COMMON/dxi/dxi
      COMMON/coor/coor
      COMMON/mater/xm1,xm2
      COMMON/epsil/epsil_r
      COMMON/L_slab/L_slab
      COMMON/slab_pos/slab_pos
      COMMON/err_max/err_max
      COMMON/grafx/grafx
      COMMON/icas/icas
      COMMON/i_d/i_d
      COMMON/ifin/ifin
      COMMON/kt/kt
      COMMON/nlpf/nlpf
      COMMON/nordre/nordre
      COMMON/nrk/nrk
      COMMON/nx/nx
      COMMON/ua/ua
      COMMON/un/un
      COMMON/ba/ba

      REAL coor(nxmax),epsil_r(nxmax),xm1,xm2
c
c---- Tableaux et variables locaux
c
c---- Entiers
      integer i,k
c
c---- Reels
      REAL dtstop,ts,tstop,xi
      REAL*4 tdeb,tfin
c----------------------------------------------------------------------
c
      WRITE(6,*) ' '
      WRITE(6,66)
      WRITE(6,*) '                          MAXWELL 1D TMz'
      WRITE(6,66)
      WRITE(6,*) ' '
c
c---- Base
c
      ba = 1

      ! c
      ! c----Parametres du calcul
      ! c
      CALL DONNEES(cfl,dt,freq,ktmax,tmax)
c
c---- Conditions initiales
c
      dt_h = dt
c
      IF (nrk .GT. 0) dt_h = 0.0

      CALL INIT_L(dt_h)
c
      ifin   = 1
      kt     = 0
      temps  = 0.0
      tstop  = 0.0
      dtstop = dt
c
      err_max =-1.0
c
c---- Initialisation des courants source
c
      tsour = temps+0.5*dt_h
c
cccc  CALL RESULT_L(dt_h, temps)
c
c---- Boucle en temps
c
c---- 1er appel a second
c
      CALL SECOND(tdeb)
c c
      ifin = 0
      xg = slab_pos
      xd = slab_pos+L_slab
      DO WHILE ((kt .LT. ktmax) .AND. (ABS(tstop-tmax) .GT. 1.0e-06))
         kt = kt+1
         temps = temps+dt
         DO i=1,nx-1
            xi = 0.5*(coor(i)+coor(i+1))
            IF ((xi .GT. xg) .AND. (xi .LT. xd)) THEN
                  epsil_r(i) = EPS_FUNC(temps)
                  ! PRINT *,epsil_r(i)
            ENDIF
         ENDDO
c
c----    dtstop et tstop : ajustes pour l'arret de la boucle 
c        n'interviennent pas dans les calculs
c
         dtstop = MIN(dtstop,tmax-tstop)
         tstop  = tstop+dtstop
c
c----    Schema saute-mouton ordre 2
c
         IF (nlpf .EQ. 2) THEN
c
c           1/ calcul du champ E a t=n*dt avec H^((n-1/2)*dt) et E^((n-1)*dt) 
c
            CALL ELECTR_2(dt,tsour,ua,un)
c
c----       2/ calcul du champ H a t=(n+1/2)*dt avec E(n*dt) et H^((n-1/2)*dt)
c
            CALL MAGNET_2(dt,temps,ua,un)
         ENDIF
c
c----    Schema saute-mouton ordre 4
c
         IF (nlpf .EQ. 4) THEN
c
c----       1/ calcul de E a t=t^n avec E^(n-1), H^(n-1/2)
c
            CALL ELECTR_4(dt,ua,un)
c
            DO k=1,ndl
               DO i=1,nx-1
                  ua(2,k,i) = ua(2,k,i)+un(2,k,i)
               ENDDO
            ENDDO
c
            CALL MAGNET_4(dt,un,un)
            CALL ELECTR_4(dt,un,un)
c
            DO k=1,ndl
               DO i=1,nx-1
                  ua(2,k,i) = ua(2,k,i)+un(2,k,i)/24.0
               ENDDO
            ENDDO
c
c----       2/ courant source d'un dipole 1-D (icas=5)
c
            IF (icas .EQ. 5) THEN
               ts = tsour
               i = i_d
c
               DO k=1,ndl
                  ua(2,k,i)   = ua(2,k,i)-dt*JCT(ts)/dxi(i)
                  ua(2,k,i-1) = ua(2,k,i-1)-dt*JCT(ts)/dxi(i-1)
               ENDDO
            ENDIF

            IF (icas .EQ. 6) THEN
                  ts = tsour
                  i = i_d
                  DO k=1,ndl
                        ua(2,k,i) = ua(2,k,i)-dt*J_SIN(ts)/dxi(i)
                        ua(2,k,i-1) = ua(2,k,i-1)-dt*J_SIN(ts)/dxi(i-1)
                  ENDDO
            ENDIF

c
c----       3/ calcul de H a t=t^(n+1/2) avec H^(n-1/2)
c           H^(n-1/2) stocke pour calcul energie
c    
            CALL MAGNET_4(dt,ua,un)
c
            DO k=1,ndl
               DO i=1,nx-1
                  bn(k,i)   = ua(1,k,i) 
                  ua(1,k,i) = ua(1,k,i)+un(1,k,i)
               ENDDO
            ENDDO
c
            CALL ELECTR_4(dt,un,un)
            CALL MAGNET_4(dt,un,un)
c
            DO k=1,ndl
               DO i=1,nx-1
                  ua(1,k,i) = ua(1,k,i)+un(1,k,i)/24.0
               ENDDO
            ENDDO
c
c----       4/ calcul energie
c
            CALL ENERGY_2(temps,ua,bn)
         ENDIF
c
c----    Schema Runge-Kutta pour nrk>=1
c
         IF (nrk .GT. 0) THEN
            CALL RKKITER(dt,temps,tsour,ua,un)
         ENDIF
c
c----    tsour : temps pour calcul du courant 
c
         tsour = temps+0.5*dt_h
c
c----    Ecriture solutions 
c
         IF ((kt .EQ. ktmax) .OR. (ABS(tstop-tmax) .LE. 1.0e-
     &      06))ifin = 1
c
         CALL RESULT_L(dt,temps)
c
c----    Fin de la boucle en temps
c
      ENDDO
c
c---- Fermeture graphique si grafx=1
c
cccc  IF (grafx .EQ. 1) CALL DISFIN
c
c---- 2eme appel a second
c
      CALL SECOND(tfin)
c     
      WRITE(6,66)
      WRITE(6,10) nordre
10    FORMAT('  FIN NORMALE MAXWELL-1D L',i1)
      WRITE(6,*) ' '
      WRITE(6,15) kt,temps
15    FORMAT(' ',i6,' ITERATIONS-','TEMPS FINAL : ',f10.4)
      WRITE(6,*) ' '
c
      WRITE(6,20) tfin-tdeb
20    FORMAT('  TEMPS CPU : ',f6.2,' secondes')
      WRITE(6,*) ' '
      WRITE(6,25) err_max
25    FORMAT('  ERREUR L2 MAX EN TEMPS : ',e15.6)
      WRITE(6,66)
c
66    FORMAT(70('='))
c
c----------------------------------------------------------------------
      STOP
      END
c----------------------------------------------------------------------
