c----------------------------------------------------------------------
      SUBROUTINE SOLPULS1_L(t_e)
c----------------------------------------------------------------------
c     Solution exacte du pulse 1-D TM (x) dans le vide
c     Cas test 1 : impulsion type gaussien
c     Base de Lagrange
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER grafx, ifin, isort, kt, mxv, nx
c
c---- Reels
      REAL t_e, x_l(ndl)
      REAL coor(nxmax), dxi(0:nxmax), ua(2,ndl,0:nxmax)
c
      COMMON/coor/coor
      COMMON/dxi/dxi
      COMMON/grafx/grafx
      COMMON/ifin/ifin
      COMMON/isort/isort
      COMMON/kt/kt
      COMMON/nx/nx
      COMMON/ua/ua
      COMMON/visu/mxv
      COMMON/x_L/x_L
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, iv, j, k
c
c---- Reels
      REAL emax, emin, erl2, som,  xi
      REAL erk(ndl), Ez(nxmax), Ez_h(nxmax), Hy(nxmax), Hy_h(nxmax),
     &     phi(ndlmax), solh(2), solp(2), solx(2), 
     &     ux(2,ndl,nxmax), xx(nxmax)
c----------------------------------------------------------------------
c
      erl2 = 0.0
c
      emin = 1.0e+08
      emax =-1.0e+08
c
      DO i=1,nx-1
         DO k=1,ndl
            xi = (coor(i+1) - coor(i))*x_L(k) + coor(i)         
c
            CALL PULSE1(solx, t_e, xi)
c
            ux(1,k,i)  = solx(1)
            ux(2,k,i)  = solx(2)
         ENDDO
c  
c----    Erreur L2
c
         som = 0.0
c
         DO iv=1,2
            DO k=1,ndl
               erk(k) = 0.0
c
               DO j=1,ndl
                  erk(k) = erk(k) + mass(k,j)*(ua(iv,j,i) - ux(iv,j,i))
               ENDDO
c
               som = som + erk(k)*(ua(iv,k,i) - ux(iv,k,i))
            ENDDO
         ENDDO
c
         erl2 = erl2 + dxi(i)*som/cmr
      ENDDO
c
      erl2 = SQRT(erl2)
c
      WRITE(14, 75) t_e/c0, erl2
c
c---- Ecriture solutions exacte et approchee 
c     Projection sur la base Pk au centre des elements
c
      DO i=1,nx-1
         xx(i) = (coor(i) + coor(i+1))/2.0
c
         CALL PK_LAGRANGE(i, xx(i), phi)
c 
         DO iv=1,2
            solh(iv) = 0.0
            solp(iv) = 0.0
c
            DO k=1,ndl
               solp(iv) = solp(iv) + phi(k)*ux(iv,k,i)
               solh(iv) = solh(iv) + phi(k)*ua(iv,k,i)
            ENDDO
         ENDDO
c
         IF (i .EQ. mxv) THEN
            WRITE(70, 100) t_e/c0, (solh(iv), iv=1,2)
            WRITE(71, 100) t_e/c0, (solp(iv), iv=1,2)
         ENDIF
c
c----    Visu solutions en x exacte et approchee si grafx=1
c
         Hy_h(i) = solh(1)
         Ez_h(i) = solh(2)
         Hy(i)   = solp(1)
         Ez(i)   = solp(2)         
c
         emin = MIN(emin, Ez(i), Ez_h(i))
         emax = MAX(emax, Ez(i), Ez_h(i))
      ENDDO
c
      IF (grafx .EQ. 1) 
     &   CALL GRAFIC(xx, Ez_h, Ez, emin, emax, nx-1, t_e)
c
c---- Ecriture solution en x a la fin des calculs (ifin=1)
c
      IF (ifin .EQ. 1) THEN
         REWIND(80)
         REWIND(81)
c
         DO i=1,nx-1
            WRITE(80, 100) xx(i), Hy_h(i), Ez_h(i)
            WRITE(81, 100) xx(i), Hy(i), Ez(i)
         ENDDO
      ENDIF
c
      IF ((ifin .EQ. 1) .OR. (MOD(kt, isort) .EQ. 0)) THEN
         WRITE(6, 50) 
         WRITE(6, *) ' Ez Min : ', emin, ' Ez Max : ', Emax
         WRITE(6, *) ' '
         WRITE(6, *) ' Erreur L2 : ', erl2, ' a ', kt, ' iterations'
         WRITE(6, 50)
      ENDIF
c
 50   FORMAT(70('-'))
 75   FORMAT(2(e15.6, 1x))
100   FORMAT(3(e15.6, 1x))
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
