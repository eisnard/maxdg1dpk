c----------------------------------------------------------------------
c                  Matrices et parametres schema P3
c                           Base Lagrange
c----------------------------------------------------------------------
c     Nombre de degres de liberte element : ndl
c     Nombre de degres de liberte face    : ndf
c----------------------------------------------------------------------
      INTEGER ndf, ndl
      PARAMETER (ndl = 4, ndf = 1)
c----------------------------------------------------------------------
c     Matrice de masse multipliee par cmr pour simplifier
c     A diviser par cmr
c
      REAL cmr
      REAL mass(ndl,ndl)
c
      DATA mass/ 
     &     128,  99, -36,  19, 
     &      99, 648, -81, -36, 
     &     -36, -81, 648,  99,
     &      19, -36,  99, 128/
c
      DATA cmr/1680/
c----------------------------------------------------------------------
c     Matrice de masse inverse multipliee par cvr pour simplifier
c     A diviser par cvr
c
      REAL cvr
      REAL amat(ndl,ndl)
c
      DATA amat/ 
     &     11664, -1836,   864, -2916, 
     &     -1836,  2224,    44,   864,
     &       864,    44,  2224, -1836,     
     &     -2916,   864, -1836, 11664/  
c
      DATA cvr/729/
c----------------------------------------------------------------------
c     Matrice des Phi_j*Dx(Phi_k) (rangement en colonne)
c     A diviser par cgr
c
      REAL cgr
      REAL rdx(ndl,ndl)
c
      DATA rdx/
     &    -40,  57,  -24,   7, 
     &    -57,   0,   81, -24,
     &     24, -81,    0,  57, 
     &     -7,  24,  -57,  40/
c
      DATA cgr/80/
c----------------------------------------------------------------------
c     Matrices de bords 
c     A diviser par cms
c
      REAL ms1(ndl,ndl), ms2(ndl,ndl), ms3(ndl,ndl), ms4(ndl,ndl)
c
      REAL cms
      DATA cms/1/
c
      DATA ms1/ 
     &     0, 0, 0, 0,
     &     0, 0, 0, 0,
     &     0, 0, 0, 0,
     &     1, 0, 0, 0/
c
      DATA ms2/ 
     &     1, 0, 0, 0,
     &     0, 0, 0, 0,
     &     0, 0, 0, 0,
     &     0, 0, 0, 0/
c
      DATA ms3/ 
     &     0, 0, 0, 0,
     &     0, 0, 0, 0,
     &     0, 0, 0, 0,
     &     0, 0, 0, 1/
c
      DATA ms4/ 
     &     0, 0, 0, 1,
     &     0, 0, 0, 0,
     &     0, 0, 0, 0,
     &     0, 0, 0, 0/
c----------------------------------------------------------------------
