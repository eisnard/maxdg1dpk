c----------------------------------------------------------------------
      SUBROUTINE E_BORDS(ua)
c----------------------------------------------------------------------
c     C.L. pour le champ electrique 
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER imet, nx
      INTEGER ifx(2), mfx(2)
c
c---- Reels
      REAL epsil_r(nxmax), flux(ndl,nxmax), ua(2,ndl,0:nx)
c
      COMMON/epsil/epsil_r
      COMMON/iinf/ifx
      COMMON/imet/imet
      COMMON/imf/mfx
      COMMON/flux/flux
      COMMON/nx/nx
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER j, k, kx
c
c---- Reels
      REAL z1, z2
      REAL flg(ndl), fld(ndl)
c----------------------------------------------------------------------
c
      kx = nx - 1
c
c---- I/ conditions de bord metalliques sur les faces frontieres
c        donnees par les mfr # 0 (r=x,y,z)
c
c---- I-1/ flux en i=1 et i=nx-1 (= kx)
c
      DO k=1,ndl
         flg(k) = 0.0
         fld(k) = 0.0
c 
         DO j=1,ndl
            flg(k) = flg(k) + ua(1,j,1 )*ms2(k,j)
            fld(k) = fld(k) + ua(1,j,kx)*ms3(k,j)
         ENDDO
c
         flux(k,1 )  = flux(k,1 ) - flg(k)*mfx(1)/cms
         flux(k,kx)  = flux(k,kx) + fld(k)*mfx(2)/cms
      ENDDO
c
c---- Si imet=1 (ie toutes les faces sont metalliques) on s'en va!
c
      IF (imet .EQ. 1) RETURN
c
c---- II/ conditions absorbantes par decentrage sur les faces
c         frontieres donnees par les ifr # 0 (r=x,y,z)
c
c---- II-1/ flux en i=1 et i=nx-1 (= kx)
c
      z1  = SQRT(epsil_r(1))
      z2  = SQRT(epsil_r(kx))
c         
      DO k=1,ndl
         flg(k) = 0.0
         fld(k) = 0.0
c
         DO j=1,ndl
            flg(k) = flg(k) + 0.5*( ua(1,j,1)  + z1*ua(2,j,1)) *ms2(k,j)
            fld(k) = fld(k) + 0.5*(-ua(1,j,kx) + z2*ua(2,j,kx))*ms3(k,j)
         ENDDO
c
         flux(k,1)  = flux(k,1)  - flg(k)*ifx(1)/cms
         flux(k,kx) = flux(k,kx) - fld(k)*ifx(2)/cms
      ENDDO
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
