c----------------------------------------------------------------------
      SUBROUTINE INIT_L(dt)
c----------------------------------------------------------------------
c---- Initialisation par defaut et appel des cas tests 
c     Base de Lagrange
c 
c---- Caracteristiques relatives du milieu: par defaut == vide
c     Selon cas test donner les epsilon (relatifs) (mu==1)
c     dans tous les cas le milieu est suppose magnetiquement parfait 
c
c     mu_r=1
c----------------------------------------------------------------------
      IMPLICIT NONE
c----------------------------------------------------------------------
      INCLUDE 'param.h'
      INCLUDE 'matrices.h'
c----------------------------------------------------------------------
c---- Tableaux et variables partagees
c
c---- Entiers
      INTEGER icas, imet, iraf, nhist, nlpf, nordre, nrk, nx
      INTEGER ifx(2), mfx(2)
c
c---- Reels
      REAL dt
      REAL coor(nxmax), epsil_r(nxmax), fib_1(ndl), fib_2(ndl), 
     &     ua(2,ndl,0:nxmax), x_L(ndl)
c
c---- Caracteres
      CHARACTER schem*10, car(0:5)*1, prec(0:5)*1
c
      COMMON/coor/coor
      COMMON/epsil/epsil_r
      COMMON/fbord/fib_1, fib_2
      COMMON/icas/icas
      COMMON/iinf/ifx
      COMMON/iraf/iraf
      COMMON/imf/mfx
      COMMON/imet/imet
      COMMON/nhist/nhist
      COMMON/nlpf/nlpf
      COMMON/nordre/nordre
      COMMON/nrk/nrk
      COMMON/nx/nx
      COMMON/ua/ua
      COMMON/x_L/x_L
c
c---- Tableaux et variables locaux
c
c---- Entiers
      INTEGER i, k
c
c---- Reels
      REAL x1, x2
c----------------------------------------------------------------------
c
c---- Calcul des noeuds de Lagrange
c
      IF (ndl .EQ. 1) THEN
         x_L(1) = 0.5
      ELSE
         DO k=1,ndl
           x_L(k) = (k - 1.0)/(ndl - 1.0)    
        ENDDO
      ENDIF
c
c---- Initialisation des parametres du milieu : epsilon (relatif) 
c
c---- Rappel: mu_r=1 
c
      DO i=1,nx
         epsil_r(i) = 1.0
      ENDDO
c
c---- Initialisation des conditions aux bords (cf e_bords et h_bords)
c
c     imet=1 ==> toutes les faces frontieres sont metalliques
c     imet=0 ==> les C.L. sont donnees par les ifk et mfk
c                     
c     ifk(i)=1 ==> la ieme face est aborbante
c     mfk(i)=1 ==> la ieme face est metallique
c     fib_k = valeurs aux extremites des fonctions de base
c
      imet = 0
c
      DO i=1,2
         ifx(i) = 0
         mfx(i) = 0
      ENDDO
c
      DO k=1,ndl
         DO i=0,nx+1
            ua(1,k,i) = 0.0                  
            ua(2,k,i) = 0.0                  
         ENDDO
      ENDDO
c
      x1 = coor(1)
      x2 = coor(nx)
c
      CALL PK_LAGRANGE(1,    x1, fib_1)
      CALL PK_LAGRANGE(nx-1, x2, fib_2)
c
c---- Les cas tests
c
c---- Propagation d'un pulse dans un milieu heterogene 
c
      IF ((icas .GE. 1) .AND. (icas .LE. 3)) CALL INIPULS_L
c
c---- Propagation d'un mode (n1,n2,n3) dans [0:1]
c
      IF (icas .EQ. 4) CALL INIMOD_L(dt)
c
c---- Courant genere par un dipole dans le vide ou une source siniosidale : tout le bord est absorbant
  
      IF (icas .EQ. 5 .OR. icas .EQ. 6) CALL INIDIPO
c
c---- Ouverture fichiers selon schema
c
      car(0) = '0'
      car(1) = '1'
      car(2) = '2'
      car(3) = '3'
      car(4) = '4'
      car(5) = '5'
c
      prec(0) = '0'
      prec(1) = '1'
      prec(2) = '2'
      prec(3) = '3'
      prec(4) = '4'
      prec(5) = '5'
c
      schem = 'L'//prec(nordre)//'_lp'//car(nlpf)//'_reg'
c
      IF (iraf .EQ. 1) 
     &   schem = 'L'//prec(nordre)//'_lp'//car(nlpf)//'_def'
      IF (iraf .EQ. 2) 
     &   schem = 'L'//prec(nordre)//'_lp'//car(nlpf)//'_raf'
c
      IF (nrk .GT. 0) THEN
         schem = 'L'//prec(nordre)//'_rk'//car(nrk)//'_reg'
c
         IF (iraf .EQ. 1) 
     &      schem ='L'//prec(nordre)//'_rk'//car(nrk)//'_def'
         IF (iraf .EQ. 2) 
     &      schem ='L'//prec(nordre)//'_rk'//car(nrk)//'_raf'
      ENDIF
c
      OPEN(UNIT=12, FILE='nrj_'//schem)
      REWIND(12)
c
c---- Ouverture fichiers pour energie exacte et erreur
c
      IF (icas .LE. 5) THEN
         OPEN(UNIT=14, FILE='err_'//schem)
         REWIND(14)
      ENDIF
c
c---- Ouverture fichiers pour solutions en temps
c
c---- Solution en temps 
c
      IF (nhist .EQ. 1) THEN
c
c----    Solution en temps au point mxv
c
         OPEN(UNIT=70, FILE=schem//'_t')
         REWIND(70)
c
         IF (icas .LE. 5) THEN 
            OPEN(UNIT=71, FILE='exa_t')
            REWIND(71)
         ENDIF
      ENDIF
c
c---- Solution en espace
c
      OPEN(UNIT=80, FILE=schem//'_x')
c
      IF (icas .LE. 5) OPEN(UNIT=81, FILE='exa_x')
c
c----------------------------------------------------------------------
      RETURN
      END
c----------------------------------------------------------------------
