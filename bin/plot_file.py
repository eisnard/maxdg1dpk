#!python
import numpy as np
import matplotlib.pyplot as plt
import os
import re
import sys
from scipy.signal import hilbert


def read_data(file_path):

	with open(file_path) as f:
		x = []
		data = []

		for line in f.readlines():
			tab = line.split()
			x.append(float(tab[0]))
			data.append(list(map(float,tab[1:])))
	name = os.path.basename(file_path)
	x,data = np.array(x),np.array(data)
	return x,data,name
# os.system("./gdL1 > /dev/null 2>&1")
x,data,name = read_data(sys.argv[1])
plt.title(os.path.basename(sys.argv[1]))
plt.plot(x,data)
plt.show()