#!python
import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.signal import hilbert
from fdtd import FDTD

c = 3e8

def read_data(file_path):

	with open(file_path) as f:
		x = []
		data = []

		for line in f.readlines():
			tab = line.split()
			x.append(float(tab[0]))
			data.append(list(map(float,tab[1:])))
	name = os.path.basename(file_path)
	x,data = np.array(x),np.array(data)
	return x,data,name

t,dataT,nameT = read_data("L1_lp2_reg_t")
x,dataX,nameX = read_data("L1_lp2_reg_x")
t,energy,nameE = read_data("nrj_L1_lp2_reg")
t/=c
with open("Data_Num") as f:
	sec2 = lambda t: 1/np.cos(t)**2
	lines = f.readlines()
	n = float(lines[2].split()[0])
	L = float(lines[2].split()[1])
	Tmax = float(lines[6].split()[0])/c
	delta = L/n
	ws = float(lines[9].split()[0])*1e9*2*np.pi
	epsr0 = float(lines[11].split()[0])
	wm = float(lines[12].split()[0])*1e9*2*np.pi
	b = float(lines[13].split()[0])
	L_slab = float(lines[14].split()[0])
	slab_pos = float(lines[15].split()[0])
	v0=3e8*1/np.sqrt(epsr0)
	eps_func = lambda t: epsr0*(1+b*np.sin(wm*t))
	source_func = lambda t: np.sin(ws*t)

	fdtd = FDTD(L,delta,Tmax,1,source_func,L/10,L_slab,slab_pos,eps_func,cfl=0.1)
	fdtd.run()

	c1 = np.sqrt(4-b**2)
	c2 = (2*np.tan(wm*t/2)+b)/c1
	c3 = np.arctan(c2)-wm*L_slab*c1/(4*v0)
	f1 = sec2(wm*t/2)/(1+c2**2)
	f2 = ws*(sec2(c3))/(1+(c1/2*np.tan(c3)-b/2)**2)
	f_ext = f1*f2/(2*np.pi)

	plt.plot(t,dataT[:,0])
	plt.title("$E_z(xo,t)$")
	plt.ylabel("$E_z(xo,t)$")
	plt.xlabel("t [s]")
	# plt.title(nameT)
	plt.show()
	xo = L_slab+slab_pos+2*delta
	fdtd.plotE1d(xo)
	freq_fdtd = fdtd.instant_freq(xo)

	plt.plot(x,dataX[:,0])
	plt.title("$E_z(x,T_{max})$")
	plt.ylabel("$E_z(x,T_{max})$")
	plt.xlabel("x [m]")
	plt.fill_betweenx([-3,3],x1=slab_pos,x2=slab_pos+L_slab,color="grey",alpha=0.8)
	plt.ylim(-3,3)
	plt.show()

	freq = np.gradient(np.unwrap(np.angle(hilbert(dataT[:,0]))),t)/(2*np.pi)
	dt = t[1]-t[0]
	n2 = int(2e-9/dt)
	n9 = int(9e-9/dt)
	plt.plot(t[n2:n9],freq_fdtd[n2:n9]/1e9,label="$f_{FDTD}$")
	plt.plot(t[n2:n9],freq[n2:n9]/1e9,label="$f_{DG}$")
	plt.plot(t[n2:n9],f_ext[n2:n9]/1e9,label="$f_{ext}$")
	plt.legend()
	plt.xlabel("t [s]")
	plt.ylabel("freq [GHz]")
	plt.show()

	# plt.plot(t,energy)
	# plt.title(nameE)
	# plt.show()