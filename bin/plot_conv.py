#!python
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from subprocess import check_output
import os
import re
from fdtd import FDTD

def read_data(file_path):

	f = open(file_path)
	t = []
	err = []

	for line in f.readlines():
		tab = line.split()
		try:
			t.append(float(tab[0]))
			err.append(float(tab[1]))
		except ValueError:
			t.append(0)
			err.append(0)
			print("Not numerical value.")
	
	name = file_path.split("_")[2]
	t,err = np.array(t),np.array(err)
	return t,err,name

n_vec = np.array([2,4,8,16,32,64])
data_path = "/home/enzo/Documents/StageInria/maxdg1DPk/bin/Data_Num"
exe_files = [file for file in os.listdir() if re.match(r"gdL.*$",file)]
exe_files.sort()
for bin in exe_files:
	err_max = []
	for n in n_vec:

		with open(data_path,"r") as f:
			data = f.readlines()
			data[2] = f"{n} 1.0 0.0     ! nb points de discretisation, longueur du domaine et origine\n"

		with open(data_path,"w") as f:
			f.writelines(data)
		
		output = check_output(["./"+bin])
		err = float(output.splitlines()[-2].split()[-1])
		err_max.append(err)
	


	
	X = 1/n_vec.reshape(-1,1)
	Y = err_max
	plt.loglog(X,Y,label="DG-$\mathbb{P}_"+bin[-1]+"$")
	plt.legend()
	plt.title("Errors as $\delta$ gets smaller")
	plt.grid(which="both",ls="--")

	m = LinearRegression()
	m.fit(np.log(X),np.log(Y))
	print(f"Taux de convergence {bin}: {m.coef_[0]}")

err_max_fdtd = []
for n in n_vec:
	cfl=1/2
	delta=1/n
	dt = cfl*delta
	H0 = lambda x: np.cos(np.pi*x)*np.sin(np.pi*dt/2)
	fdtd = FDTD(1,delta,10,1,lambda t: 0,0,1,0,lambda t: 1,lambda x : np.sin(np.pi*x),H0,cfl=cfl,boundary_condition="PEC",eps_0=1,mu_0=1)
	fdtd.run(False,False)
	# fdtd.anim1d(-3/2,3/2)
	t2 = np.linspace(0,fdtd.T_max,fdtd.nt)
	x2 = np.linspace(0,fdtd.L,fdtd.n_space)
	X,T = np.meshgrid(x2,t2)
	E_ext = np.sin(np.pi*X)*np.cos(np.pi*T)
	err_fdtd = np.linalg.norm(E_ext-fdtd.Ez,axis=1)*np.sqrt(fdtd.delta)
	err_max_fdtd.append(np.max(err_fdtd))

X = 1/n_vec.reshape(-1,1)
Y = err_max_fdtd
plt.loglog(X,Y,label="FDTD")
plt.legend()
plt.xlabel("$\delta$")
plt.ylabel("Error")
m.fit(np.log(X),np.log(Y))
print(f"Taux de convergence FDTD: {m.coef_[0]}")




plt.show()
