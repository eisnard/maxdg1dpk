#!python
import numpy as np
import matplotlib.pyplot as plt
import os
import re
from fdtd import FDTD

delta = 1/50
cfl=1/2
dt = cfl*delta
# H0 = lambda x : 0
H0 = lambda x: np.cos(np.pi*x)*np.sin(np.pi*dt/2)
fdtd = FDTD(1,delta,10,1,lambda t: 0,0,1,0,lambda t: 1,lambda x : np.sin(np.pi*x),H0,cfl=cfl,boundary_condition="PEC",eps_0=1,mu_0=1)
fdtd.run()
# fdtd.anim1d(-3/2,3/2)
t2 = np.linspace(0,fdtd.T_max,fdtd.nt)[1:]
energy_fdtd = fdtd.energy()
# print(energy_fdtd)

def plot_data(file_path):

	f = open(file_path)
	t = []
	e = []

	for line in f.readlines():
		tab = line.split()
		try:
			t.append(float(tab[0]))
			e.append(float(tab[1]))
		except ValueError:
			t.append(0)
			e.append(0)
			print("Not numerical value.")
	
	name = file_path.split("_")[1]+"_"+file_path.split("_")[2]
	plt.plot(t,e,label=name)
	t,e = np.array(t),np.array(e)
	return t,e

files = [f for f in os.listdir() if re.match(r"nrj.*$",f) ]
for f in files:
	print(f)
	t,e = plot_data(f)
plt.plot(t2,1/4*np.ones(t2.shape),label="ext_energy")
plt.plot(t2,energy_fdtd,label="energy_fdtd")
plt.title(f"Numerical Energy with $\delta$={delta} and clf={cfl}")
plt.ylabel("$\mathcal{E}_{num}$(t)")
plt.xlabel("t")
plt.legend()
plt.show()