#!python
import numpy as np
import matplotlib.pyplot as plt
import re
import os
from fdtd import FDTD

os.system("./clean")
c = 3e8

exe_files = [file for file in os.listdir() if re.match(r"gdL.*$",file)]
for bin in exe_files:
	os.system("./"+bin)
def read_data(file_path):

	f = open(file_path)
	x = []
	H = []
	E = []

	for line in f.readlines():
		tab = line.split()
		try:
			x.append(float(tab[0]))
			H.append(float(tab[1]))
			E.append(float(tab[2]))
		except ValueError:
			x.append(0)
			H.append(0)
			E.append(0)
			print("Not numerical value.")
	
	name = "_".join(file_path.split("_")[:2])
	x,E,H = np.array(x),np.array(E),np.array(H)
	return x,E,H,name

files_x = [file for file in os.listdir() if re.match(r"L.*_x$",file)]
files_t = [file for file in os.listdir() if re.match(r"L.*_t$",file)]
E_vec_x = []
H_vec_x = []
E_vec_t = []
H_vec_t = []
name_vec = []
for f in files_x:
	x,E,H,name = read_data(f)
	E_vec_x.append(E)
	H_vec_x.append(H)
	name_vec.append(name)
for f in files_t:
	t,E,H,name = read_data(f)
	E_vec_t.append(E)
	H_vec_t.append(H)
name_vec.append("fdtd")
name_vec.append("ext")
name_vec.append("ext2")
cfl=0.1
delta=1/32
dt = cfl*delta
fdtd = FDTD(1,delta,4,1,lambda t: 0,0,1,0,lambda t: 1,lambda x :np.sin(np.pi*x),lambda x: np.cos(np.pi*x)*np.sin(dt/2),cfl=cfl,boundary_condition="PEC",eps_0=1,mu_0=1)
fdtd.run(False,False)
x2 = np.linspace(0,1,fdtd.n_space)
x3 = x2[:-1]+fdtd.delta/2
t2 = np.linspace(0,fdtd.T_max,fdtd.nt)
t3 = t2+fdtd.dt/2

ko = int(fdtd.n_space*0.8)
x,E_ext_x,H_ext_x,name = read_data("exa_x")
E_ext_x2,H_ext_x2 = np.sin(np.pi*x2)*np.cos(np.pi*fdtd.T_max),np.cos(np.pi*x3)*np.sin(np.pi*t3[-1])
t,E_ext_t,H_ext_t,_ = read_data("exa_t")
E_ext_t2,H_ext_t2 = np.sin(np.pi*x2[ko])*np.cos(np.pi*t2),np.cos(np.pi*x3[ko])*np.sin(np.pi*t3)
t*=c
plt.title("$E_z(T_{max},x)$")
plt.plot(x,np.array(E_vec_x).T)
plt.plot(x2,fdtd.Ez[-1])
plt.plot(x,E_ext_x)
plt.plot(x2,E_ext_x2)
plt.legend([name+"_E" for name in name_vec])
plt.show()
plt.title("$H_y(T_{max},x)$")
plt.plot(x,np.array(H_vec_x).T)
plt.plot(x3,fdtd.Hy[-1])
plt.plot(x,H_ext_x)
plt.plot(x3,H_ext_x2)
plt.legend([name+"_H" for name in name_vec])
plt.show()

plt.title("$E_z(t,1/2)$")
plt.plot(t,np.array(E_vec_t).T)

plt.plot(t2,fdtd.Ez[:,ko])
plt.plot(t,E_ext_t)
plt.plot(t2,E_ext_t2)
plt.legend([name+"_E" for name in name_vec])
plt.show()
plt.title("$H_y(t,1/2)$")
plt.plot(t,np.array(H_vec_t).T)
plt.plot(t3,fdtd.Hy[:,ko])
plt.plot(t,H_ext_t)
plt.plot(t3,H_ext_t2)
plt.legend([name+"_H" for name in name_vec])
plt.show()