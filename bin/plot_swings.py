#!python
import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.signal import hilbert
from fdtd import FDTD,progress
import os

c = 3e8

def read_data(file_path):

	with open(file_path) as f:
		x = []
		data = []

		for line in f.readlines():
			tab = line.split()
			x.append(float(tab[0]))
			data.append(list(map(float,tab[1:])))
	name = os.path.basename(file_path)
	x,data = np.array(x),np.array(data)
	return x,data,name

def f_ext_func(t,epsr0,wm,ws,L_slab):
	v0=c*1/np.sqrt(epsr0)
	c1 = np.sqrt(4-b**2)
	c2 = (2*np.tan(wm*t/2)+b)/c1
	c3 = np.arctan(c2)-wm*L_slab*c1/(4*v0)
	f1 = sec2(wm*t/2)/(1+c2**2)
	f2 = ws*(sec2(c3))/(1+(c1/2*np.tan(c3)-b/2)**2)
	f_ext = f1*f2/(2*np.pi)
	return f_ext

L_vec = np.linspace(0, 350e-3, 30)

swing_ext = []
swing_fdtd = []
swing_dg = []
i=0
for L_slab in L_vec:
	progress(i,30)
	i+=1
	f = open("Data_Num")
	lines = f.readlines()
	lines[14] = f"{L_slab}      ! Longueur du diélectrique s'il y en a un\n"
	f.close()
	f = open("Data_Num","w")
	f.writelines(lines)
	f.close()
	os.system("./gdL1 > /dev/null 2>&1")

	t,dataT,nameT = read_data("L1_lp4_reg_t")
	x,dataX,nameX = read_data("L1_lp4_reg_x")
	t,energy,nameE = read_data("nrj_L1_lp4_reg")
	t/=c
	dt = t[1]-t[0]
	n2 = int(2e-9/dt)
	n9 = int(9e-9/dt)
	with open("Data_Num") as f:
		sec2 = lambda t: 1/np.cos(t)**2
		lines = f.readlines()
		n = float(lines[2].split()[0])
		L = float(lines[2].split()[1])
		Tmax = float(lines[6].split()[0])/c
		delta = L/n
		ws = float(lines[9].split()[0])*1e9*2*np.pi
		epsr0 = float(lines[11].split()[0])
		wm = float(lines[12].split()[0])*1e9*2*np.pi
		b = float(lines[13].split()[0])
		# L_slab = float(lines[14].split()[0])
		slab_pos = float(lines[15].split()[0])
		
		f_ext = f_ext_func(t,epsr0,wm,ws,L_slab)

		eps_func = lambda t: epsr0*(1+b*np.sin(wm*t))
		source_func = lambda t: np.sin(ws*t)

		fdtd = FDTD(L,delta,Tmax,1,source_func,L/10,L_slab,slab_pos,eps_func,cfl=0.5)
		xo = L_slab+slab_pos+2*delta
		fdtd.run(False,False)
		f_fdtd = fdtd.instant_freq(xo)
		f_dg = np.gradient(np.unwrap(np.angle(hilbert(dataT[:,0]))),t)/(2*np.pi)
		swing_ext.append(np.max(f_ext[n2:n9])-np.min(f_ext[n2:n9]))
		swing_fdtd.append(np.max(f_fdtd[n2:n9])-np.min(f_fdtd[n2:n9]))
		swing_dg.append(np.max(f_ext[n2:n9])-np.min(f_ext[n2:n9]))

L_vec*=1e3
swing_dg,swing_ext,swing_fdtd = np.array(swing_dg),np.array(swing_ext),np.array(swing_fdtd)
plt.title(f"Frequencies swings for various slab lengths for b={b} and $\delta$=1.5 mm")
plt.xlabel("Width of slab [mm]")
plt.ylabel("Swing of instantaneous frequency [GHz]")
plt.plot(L_vec,swing_ext/1e9,label="Exact swing")
plt.plot(L_vec,swing_dg/1e9,label="swing with DG L1 lp4")
plt.plot(L_vec,swing_fdtd/1e9,label="swing with fdtd")
plt.legend()
plt.show()

		