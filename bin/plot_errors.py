#!python
import numpy as np
import matplotlib.pyplot as plt
import os
import re
from fdtd import FDTD
import path

c = 3e8
delta = 1/50
cfl=1/2
dt = cfl*delta
H0 = lambda x: np.cos(np.pi*x)*np.sin(np.pi*dt/2)
fdtd = FDTD(1,delta,10,1,lambda t: 0,0,1,0,lambda t: 1,lambda x : np.sin(np.pi*x),H0,cfl=cfl,boundary_condition="PEC",eps_0=1,mu_0=1)
fdtd.run()
# fdtd.anim1d(-3/2,3/2)
t2 = np.linspace(0,fdtd.T_max,fdtd.nt)
x2 = np.linspace(0,fdtd.L,fdtd.n_space)
X,T = np.meshgrid(x2,t2)
E_ext = np.sin(np.pi*X)*np.cos(np.pi*T)
err_fdtd = np.linalg.norm(E_ext-fdtd.Ez,axis=1)*np.sqrt(fdtd.delta)

def read_data(file_path):

	f = open(file_path)
	t = []
	err = []

	for line in f.readlines():
		tab = line.split()
		try:
			t.append(float(tab[0]))
			err.append(float(tab[1]))
		except ValueError:
			t.append(0)
			err.append(0)
			print("Not numerical value.")
	
	t,err = c*np.array(t),np.array(err)
	return t,err,os.path.basename(file_path)

files = [f for f in os.listdir() if re.match(r"err.*$",f) ]
for f in files:
	t,err,name = read_data(f)
	plt.plot(t,err,label=name)
	print(f)
	print(f"err max:{np.max(err)}\n")
plt.plot(t2,err_fdtd,label="err_fdtd",ls="-")
plt.title(f"Errors with $\delta$={delta} and cfl={cfl}")
plt.xlabel("t")
plt.ylabel("$||E-E_{ext}||$")
plt.legend()
plt.show()



