FC = gfortran
FFLAGS = -g -fcheck=bounds -Wall
SRC = $(wildcard src/*.f)
OBJ = $(patsubst src/%,obj/%,$(SRC:.f=.o))
ORDRE = 1
BIN = bin/gdL$(ORDRE)

all: $(BIN)

obj/%.o: src/%.f src/matrices.h
	$(FC) -DORDRE=$(ORDRE) $(FFLAGS) -c $< -o $@

src/matrices.h: src/matrices_L$(ORDRE).h
	cp $< $@

$(BIN): $(OBJ)
	$(FC) -DORDRE=$(ORDRE) $(FFLAGS) $^ -o $@


1:
	make clean_dep
	make ORDRE=1

2:
	make clean_dep
	make ORDRE=2

3:
	make clean_dep
	make ORDRE=3

4:
	make clean_dep
	make ORDRE=4

5:
	make clean_dep
	make ORDRE=5

clean_output:
	rm -f bin/*_rk* bin/*_lp* bin/*_x* bin/*_t* bin/Mesh*

clean_exec:
	rm -f bin/gdL*

clean_dep:
	rm -f obj/*.o src/matrices.h

clean: clean_output clean_exec clean_dep

tar.gz: clean
	tar -czvf maxdg1DPk.tar.gz bin src obj .gitignore Makefile
zip: clean
	zip -r maxdg1DPk.zip bin src obj .gitignore Makefile